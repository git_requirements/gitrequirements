package com.agh.rest.requirements;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;

@Path("/reqgetxml")
public class ReqGetterBuild {

	// @GET
	// @Produces(MediaType.TEXT_HTML)
	// public String sayHtmlHello(@QueryParam("project") String project,
	// @QueryParam("release") String release,
	// @QueryParam("level") String level) throws Exception {
	//
	// System.out.println(project);
	// System.out.println(release);
	// System.out.println(level);
	//
	// if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQ)) {
	// ReqGetter RG = new ReqGetter();
	// RG.getReqs();
	// System.out.println("Requirements list collected");
	//
	// }
	//
	// return "<html> " + "<title>" + "requirement" + "</title>" + "<body><h1>"
	// + "requirement: " + project + " "
	// + release + " " + level + "</body></h1>" + "</html> ";
	// }

	// tutaj powinno dzia�a� wyszukiwanie wymaga� i zbieranie listy

	// @GET
	// @Produces("application/json")
	// @Path("{project}/{release}/{level}")
	// public ReqGetterTest getReq(@QueryParam("project") String project,
	// @QueryParam("release") String release,
	// @QueryParam("level") String level) throws Exception {
	// ReqGetterTest RG = new ReqGetterTest();
	// RG.getReqsTest(project, release, level);
	// System.out.println("Requirements list collected");
	// return RG;
	// }

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/path")
	public JSONObject getReq() {
		JSONObject obj = new JSONObject();
		obj.put("project", "lala");
		obj.put("release", "baba");
		obj.put("level", "nic");
		return obj;
	}

}