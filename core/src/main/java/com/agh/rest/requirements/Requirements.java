package com.agh.rest.requirements;

import java.nio.file.Files;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.req.ReqGetter;

// Plain old Java Object it does not extend as class or implements 
// an interface

// The class registers its methods for the HTTP GET request using the @GET annotation. 
// Using the @Produces annotation, it defines that it can deliver several MIME types,
// text, XML and HTML. 

// The browser requests per default the HTML MIME type.

//Sets the path to base URL + /hello
@Path("/requirements")
public class Requirements {

	// This method is called if TEXT_PLAIN is request
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String sayPlainTextHello() {
		return "requirement";
	}

	// This method is called if XML is request
	@GET
	@Produces(MediaType.TEXT_XML)
	@Path("{project}/{release}/{level}")
	public String sayXMLHello(@PathParam("project") String project, @PathParam("release") String release,
			@PathParam("level") String level) {
		return "<?xml version=\"1.0\"?>" + "<requirement> requirement: " + project + " " + release + " " + level
				+ "</requirement>";
	}

	// This method is called if HTML is request
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String sayHtmlHello(@QueryParam("project") String project, @QueryParam("release") String release,
			@QueryParam("level") String level) throws Exception {

		System.out.println(project);
		System.out.println(release);
		System.out.println(level);

		if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQ)) {
			ReqGetter RG = new ReqGetter();
			RG.getReqs();
			System.out.println("Requirements list collected");

		}

		return "<html> " + "<title>" + "requirement" + "</title>" + "<body><h1>" + "requirement: " + project + " "
				+ release + " " + level + "</body></h1>" + "</html> ";
	}

	// // This method is called if HTML is request
	// @GET
	// @Path("{project}/{release}/{level}")
	// @Produces(MediaType.TEXT_HTML)
	// public String sayHtmlHello(@PathParam("project") String project,
	// @PathParam("release") String release,
	// @PathParam("level") String level) throws Exception {
	//
	// System.out.println(project);
	// System.out.println(release);
	// System.out.println(level);
	//
	// if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQ)) {
	// ReqGetter RG = new ReqGetter();
	// RG.getReqs();
	// System.out.println("Requirements list collected");
	//
	// }
	//
	// return "<html> " + "<title>" + "requirement" + "</title>" + "<body><h1>"
	// + "requirement" + "</body></h1>"
	// + "</html> ";
	// }

}