package com.agh.rest.requirements;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.lib.Repository;
import org.json.simple.JSONObject;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.req.Level;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqFields;
import com.agh.reqs.req.ReqResource;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ReqGetterTest {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	FolderCleaner FC;
	ReqResource RR;
	Level level;
	Req getReqs;

	public ReqGetterTest() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		RR = new ReqResource();
		FC = new FolderCleaner();
	}

	public JSONObject getReqsTest(String project, String release, String level) throws Exception {
		try {

			getReqs = RR.getReqForReqGetter();
			ArrayList<String> projects = getReqs.getProjects();
			ArrayList<String> releases = getReqs.getReleases();
			BM.changeBranch(release);
			Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), project, level);

			File fileList = new File(path.toString());
			File[] listOfFiles = fileList.listFiles();
			ArrayList<Long> lista = new ArrayList<>();
			List l1 = new LinkedList();

			collectReqsTest(level, project, listOfFiles, lista, l1);
			String listName = project + level + release;
			JSONObject jsonObject = Utils.createListOfFileTest(l1, listName);
			GC.commitAll("take list of req in " + listName);
			return jsonObject;

		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQ.toString()));
		}
	}

	public void getReqs() throws Exception {
		try {
			getReqs = RR.getReqForReqGetter();
			level = getReqs.getLevel();
			ArrayList<String> projects = getReqs.getProjects();
			ArrayList<String> releases = getReqs.getReleases();
			for (String release : releases) {
				BM.changeBranch(release);
				for (String project : projects) {
					Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), project, level.getName());

					File fileList = new File(path.toString());
					File[] listOfFiles = fileList.listFiles();
					ArrayList<Long> lista = new ArrayList<>();
					List l1 = new LinkedList();

					collectReqs(level, project, listOfFiles, lista, l1);
					String listName = project + level + release;
					Utils.createListOfFile(l1, listName);
					GC.commitAll("take list of req in " + listName);
				}
			}
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQ.toString()));
		}
	}

	public void getReqsWithTag() throws Exception {
		try {
			getReqs = RR.getReqForReqGetterTags();
			level = getReqs.getLevel();
			ArrayList<String> projects = getReqs.getProjects();
			ArrayList<String> releases = getReqs.getReleases();
			ArrayList<String> tags = getReqs.getTags();
			for (String release : releases) {

				BM.changeBranch(release);
				for (String project : projects) {
					Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), project, level.getName());

					File fileList = new File(path.toString());
					File[] listOfFiles = fileList.listFiles();
					ArrayList<Long> lista = new ArrayList<>();
					List l1 = new LinkedList();

					collectReqsTag(level, project, listOfFiles, lista, l1, tags);
					String listName = "tags" + project + level + release;
					Utils.createListOfFile(l1, listName);
					GC.commitAll("take list of req with searching tags in " + listName);
				}

			}
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQFROMTAG.toString()));
		}
	}

	private void collectReqs(Level level, String projects, File[] listOfFiles, ArrayList<Long> lista, List l1) {
		for (int i = 0; i < listOfFiles.length; i++) {
			Req req = RR.getReqForGet(projects, level.toString(), listOfFiles[i].getName());
			long id = req.getId();
			lista.add(id);
			Map a = new LinkedHashMap();

			a.put(ReqFields.ID.getName(), req.getId());
			a.put(ReqFields.NAME.getName(), req.getName());
			l1.add(a);
		}
	}

	private void collectReqsTest(String level, String projects, File[] listOfFiles, ArrayList<Long> lista, List l1) {
		for (int i = 0; i < listOfFiles.length; i++) {
			Req req = RR.getReqForGet(projects, level.toString(), listOfFiles[i].getName());
			long id = req.getId();
			lista.add(id);
			Map a = new LinkedHashMap();

			a.put(ReqFields.ID.getName(), req.getId());
			a.put(ReqFields.NAME.getName(), req.getName());
			l1.add(a);
		}
	}

	private void collectReqsTag(Level level, String projects, File[] listOfFiles, ArrayList<Long> lista, List l1,
			ArrayList<String> tagsGet) {
		for (int i = 0; i < listOfFiles.length; i++) {
			Req req = RR.getReqForGet(projects, level.toString(), listOfFiles[i].getName());
			long id = req.getId();
			ArrayList<String> tags = req.getTags();
			for (String tag : tags) {
				for (String tagGet : tagsGet) {
					if (tag.equals(tagGet)) {
						lista.add(id);
						Map a = new LinkedHashMap();

						a.put(ReqFields.ID.getName(), req.getId());
						a.put(ReqFields.NAME.getName(), req.getName());
						a.put(ReqFields.TAGS.getName(), req.getTags());
						l1.add(a);
					}
				}
			}
		}
	}
}
