package com.agh.reqs.tags;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.lib.Repository;
import org.json.simple.parser.ParseException;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqFields;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class TagsGetter {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	TagsResource TR;
	FolderCleaner FC;
	Req getTagsList;

	public TagsGetter() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		TR = new TagsResource();
		FC = new FolderCleaner();
	}

	public void getTags() throws Exception {
		try {

			getTagsList = TR.getReqForTagsRemover();
			ArrayList<String> releases = getTagsList.getReleases();
			BM.changeBranch("develop");
			Path path = Paths.get(ReqsConfig.REPOSITORY_TAGS.toString());

			for (String release : releases) {
				File folder = new File(path.toString());
				File[] listOfFiles = folder.listFiles();
				ArrayList<Long> lista = new ArrayList<>();
				List l1 = new LinkedList();

				collectTags(listOfFiles, lista, l1);
				Utils.createListOfFile(l1, "ListOfTags" + release);
				GC.commitAll("take list of tags on " + release + " release");
				BM.changeBranch("develop");
			}
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_TAG.toString()));
		}
	}

	private void collectTags(File[] listOfFiles, ArrayList<Long> lista, List l1)
			throws FileNotFoundException, IOException, ParseException {
		for (int i = 0; i < listOfFiles.length; i++) {
			Req req = TR.getTagForTagGetter(listOfFiles[i].getName());
			long id = req.getId();
			lista.add(id);
			Map a = new LinkedHashMap();

			a.put(ReqFields.ID.getName(), req.getId());
			a.put(ReqFields.NAME.getName(), req.getName());
			l1.add(a);
		}
	}
}
