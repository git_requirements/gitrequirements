package com.agh.reqs.tags;

import java.nio.file.Paths;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.req.IdGenerator;

public class TagsIdGenerator extends IdGenerator {

	public TagsIdGenerator() {
		super(Paths.get(ReqsConfig.REPOSITORY_IDCOUNT_TAGS.toString()));
	}

	public long getId() throws Exception {
		return super.getId();
	}
}
