package com.agh.reqs.tags;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.req.Req;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class TagsRemover {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	TagsResource TR;
	FolderCleaner FC;
	Req tagRemove;

	public TagsRemover() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		TR = new TagsResource();
		FC = new FolderCleaner();
	}

	public void removeTag() throws Exception {
		try {
			tagRemove = TR.getReqForTagsRemover();
			String name = tagRemove.getName();
			ArrayList<String> releases = tagRemove.getReleases();
			for (String release : releases) {
				BM.changeBranch(release);
				remove(name);
				GC.commitAll("removed tag " + name);
				BM.changeBranch("develop");
			}

		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_TAG.toString()));
		}
	}

	public void remove(String name) {
		try {
			Path pathTagToRemove = Paths.get(ReqsConfig.REPOSITORY_TAGS.toString(), name + ".json");
			if (Files.exists(pathTagToRemove)) {
				Files.delete(pathTagToRemove);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
