package com.agh.reqs.tags;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.req.Req;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class TagsEditor {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	FolderCleaner FC;
	TagsAdder TA;
	TagsResource TRS;
	TagsRemover TR;
	Req tagEdit, tagToEdit;

	public TagsEditor() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		TRS = new TagsResource();
		TA = new TagsAdder();
		TR = new TagsRemover();
		FC = new FolderCleaner();
	}

	public void editTag() throws Exception {
		try {
			tagEdit = TRS.getReqForTagEditor();
			ArrayList<String> releases = tagEdit.getReleases();
			String name = tagEdit.getName();
			String oldName = tagEdit.getOldName();
			for (String release : releases) {
				BM.changeBranch(release);
				tagToEdit = TRS.getTag(oldName);
				long tagId = tagToEdit.getId();
				TR.remove(oldName);
				GC.commitAll("remove old ");
				TA.createTag(tagId, name, release);
				Path addTagPath = Paths.get(ReqsConfig.REPOSITORY_TAGS.toString(), name + ".json");
				GC.gitAddAll();
				GC.commitAll("tag " + oldName + " edited to " + name);
				BM.changeBranch("develop");
			}
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_TAG.toString()));
		}
	}
}
