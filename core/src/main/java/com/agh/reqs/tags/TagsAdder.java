package com.agh.reqs.tags;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.eclipse.jgit.lib.Repository;
import org.json.simple.JSONObject;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqFields;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class TagsAdder {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	FolderCleaner FC;
	TagsIdGenerator TIG;
	TagsResource TR;
	Req tagAdd;

	public TagsAdder() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		TIG = new TagsIdGenerator();
		TR = new TagsResource();
		FC = new FolderCleaner();
	}

	public void addTag() throws Exception {
		try {

			tagAdd = TR.getReqForTagAdder();
			String name = tagAdd.getName();

			ArrayList<String> releases = tagAdd.getReleases();
			for (String release : releases) {
				BM.changeBranch(release);
				tagAdd.setId(TIG.getId());
				long id = tagAdd.getId();
				if (!Files.exists(ReqsConfig.REPOSITORY_TAGS)) {
					Files.createDirectory(ReqsConfig.REPOSITORY_TAGS);
				}
				if (Files.exists(Paths.get(ReqsConfig.REPOSITORY_TAGS.toString()))) {
					createTag(id, name, release);
					GC.gitAddAll();
					GC.commitAll("add new tag " + name);
					BM.changeBranch("develop");
				}
			}
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_ADD_TAG.toString()));
		}
	}

	public void createTag(long id, String name, String release) {
		Path pathTag = Paths.get(ReqsConfig.REPOSITORY_TAGS.toString());
		JSONObject obj = new JSONObject();
		obj.put(ReqFields.NAME.getName(), name);
		obj.put(ReqFields.ID.getName(), id);
		obj.put(ReqFields.RELEASE.getName(), release);

		createTagFile(name, pathTag, obj);
	}

	private static void createTagFile(String name, Path pathTag, JSONObject obj) {
		try {
			Path path = Paths.get(pathTag.toString(), name + ".json");
			if (!Files.exists(path)) {
				FileWriter file = new FileWriter(path.toFile());
				file.write(obj.toJSONString());
				file.flush();
				file.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
