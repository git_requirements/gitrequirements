package com.agh.reqs.tags;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.project.ProjectResource;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqFields;
import com.agh.reqs.util.Utils;

public class TagsResource {
	public Req getReqForTagAdder() throws FileNotFoundException, IOException, ParseException {
		Path pathAddTag = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_ADD_TAG.toString());
		return getIdAndName(pathAddTag);
	}

	public Req getReqForTagEditor() throws FileNotFoundException, IOException, ParseException {
		Path pathEditTag = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_TAG.toString());
		return getIdAndNameAndOldName(pathEditTag);
	}

	public Req getTag(String tagName) throws FileNotFoundException, IOException, ParseException {
		Path pathGetTag = Paths.get(ReqsConfig.REPOSITORY_TAGS.toString(), tagName + ".json");
		return getStringRelease(pathGetTag);
	}

	public Req getTagForTagGetter(String tagName) throws FileNotFoundException, IOException, ParseException {
		Path addFilePath = Paths.get(ReqsConfig.REPOSITORY_TAGS.toString(), tagName);
		return getIdAndName(addFilePath);
	}

	public Req getReqForTagsRemover() throws FileNotFoundException, IOException, ParseException {
		Path addFilePath = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_TAG.toString());
		return getOnlyName(addFilePath);
	}

	public Req getReqForTagGetter() throws FileNotFoundException, IOException, ParseException {
		Path pathAddTag = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_TAG.toString());
		return getRelease(pathAddTag);
	}

	private Req getOnlyName(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);
		ArrayList<String> release = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());

		Req editedReq = new Req(0);
		editedReq.setReleases(release);
		ProjectResource.getName(jsonObject, editedReq);

		return editedReq;
	}

	private Req getIdAndName(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);
		long id = (long) jsonObject.get(ReqFields.ID.getName());
		ArrayList<String> release = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());

		Req editedReq = new Req(id);
		editedReq.setReleases(release);
		ProjectResource.getName(jsonObject, editedReq);
		return editedReq;
	}

	private Req getStringRelease(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);
		long id = (long) jsonObject.get(ReqFields.ID.getName());
		String release = (String) jsonObject.get(ReqFields.RELEASE.getName());

		Req editedReq = new Req(id);
		editedReq.setRelease(release);
		ProjectResource.getName(jsonObject, editedReq);

		return editedReq;
	}

	private Req getRelease(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);

		Req editedReq = new Req(0);
		ArrayList<String> release = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());
		editedReq.setReleases(release);

		return editedReq;
	}

	private Req getIdAndNameAndOldName(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);

		String oldName = (String) jsonObject.get(ReqFields.OLDNAME.getName());
		ArrayList<String> release = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());

		Req editedReq = new Req(0);
		editedReq.setOldName(oldName);
		editedReq.setReleases(release);
		ProjectResource.getName(jsonObject, editedReq);

		return editedReq;
	}
}
