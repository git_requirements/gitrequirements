package com.agh.reqs.util;

import java.nio.file.Files;
import java.nio.file.Path;

public class FolderCleaner {
	public void cleanFolderReq(Path path) {
		try {
			Files.delete(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
