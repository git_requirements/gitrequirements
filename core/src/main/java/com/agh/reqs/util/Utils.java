package com.agh.reqs.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.lib.Repository;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.project.ProjectResource;
import com.agh.reqs.repo.RepositoryCreator;
import com.agh.reqs.req.Level;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqFields;

public class Utils {
	public static Path toPath(Req req) {
		return Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), req.getProjects().get(0).toString(),
				req.getLevel().getName(), req.getId() + ".json");
	}

	public static Repository takeThisRepo() throws IOException {
		RepositoryCreator repoCreator = new RepositoryCreator();
		Repository repository = repoCreator.createEmptyRepository();
		return repository;
	}

	public static void createListOfFile(List l1, String name) throws IOException {
		JSONObject obj = new JSONObject();
		obj.put("Files", l1);

		Path pathToList = Paths.get(ReqsConfig.REPOSITORY_RESULTS.toString(), name + ".json");
		FileWriter file = new FileWriter(pathToList.toFile());
		file.write(obj.toJSONString());
		file.flush();
		file.close();
	}

	public static JSONObject createListOfFileTest(List l1, String name) throws IOException {
		JSONObject obj = new JSONObject();
		obj.put("Files", l1);

		return obj;
	}

	public static void collectProject(File[] listOfFiles, ArrayList<Long> lista, List l1)
			throws FileNotFoundException, IOException, ParseException {
		for (int i = 0; i < listOfFiles.length; i++) {
			Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), listOfFiles[i].getName(),
					"projectDesc" + ".json");
			if (Files.exists(path)) {
				ProjectResource PR = new ProjectResource();
				Req req = PR.getReqFromProject(listOfFiles[i].getName());
				long id = req.getId();
				lista.add(id);
				Map a = new LinkedHashMap();

				a.put(ReqFields.ID.getName(), req.getId());
				a.put(ReqFields.NAME.getName(), req.getName());
				l1.add(a);
			}
		}
	}

	public static JSONObject getResourceCheck(Path tempFilePath) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try (FileReader reader = new FileReader(tempFilePath.toString())) {
			obj = parser.parse(reader);
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		JSONObject jsonObject = (JSONObject) obj;
		return jsonObject;
	}

	public static void createFile(JSONObject obj, Path path) throws IOException {
		FileWriter file = new FileWriter(path.toFile());
		file.write(obj.toJSONString());
		file.flush();
		file.close();
	}

	public static List<String> createLevelList() {
		List<String> lista = new ArrayList<String>();
		lista.add(Level.L1.getName());
		lista.add(Level.L2.getName());
		lista.add(Level.L3.getName());
		lista.add(Level.L4.getName());
		return lista;
	}
}
