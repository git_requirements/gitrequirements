package com.agh.reqs.repo;

import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import com.agh.reqs.config.ReqsConfig;

public class RepositoryCreator {
	Repository repo;

	public Repository createEmptyRepository() throws IOException {
		if (isRepositoryCreated()) {
			return repo;
		} else {
			repo = FileRepositoryBuilder.create(ReqsConfig.GIT_PATH.toFile());
			repo.create();
			return repo;
		}
	}

	public boolean isRepositoryCreated() throws IOException {
		if (repo == null) {
			repo = FileRepositoryBuilder.create(ReqsConfig.GIT_PATH.toFile());
		}
		return repo.getObjectDatabase().exists();
	}

	public void deleteRepository() throws IOException {
		if (Files.exists(ReqsConfig.STORAGE_PATH)) {
			FileUtils.deleteDirectory(ReqsConfig.GIT_PATH.toFile());
		}
	}
}
