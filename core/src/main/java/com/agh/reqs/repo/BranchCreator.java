package com.agh.reqs.repo;

import java.io.IOException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRefNameException;
import org.eclipse.jgit.api.errors.RefAlreadyExistsException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.lib.Repository;

public class BranchCreator {
	private Repository repo;
	private Git git;

	public BranchCreator(Repository repository) {
		this.repo = repository;
		git = new Git(repo);
	}

	public void addBranch(String branchName) throws RefAlreadyExistsException, RefNotFoundException,
			InvalidRefNameException, GitAPIException, IOException {
		git.branchCreate().setName(branchName).call();
	}
}
