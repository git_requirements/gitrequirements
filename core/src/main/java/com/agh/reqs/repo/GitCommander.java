package com.agh.reqs.repo;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoFilepatternException;
import org.eclipse.jgit.lib.Repository;

public class GitCommander {
	private Git git;
	private Repository repo;

	public GitCommander(Repository repository) {
		this.repo = repository;
		this.git = new Git(repo);
	}

	public void gitAddAll() throws NoFilepatternException, GitAPIException {
		git.add().addFilepattern(".").call();
	}

	public void commitAll(String message) throws Exception {
		git.commit().setAll(true).setMessage(message).call();
	}
}
