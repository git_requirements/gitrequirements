package com.agh.reqs.repo;

import java.nio.file.Path;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRefNameException;
import org.eclipse.jgit.api.errors.RefAlreadyExistsException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;

public class BranchCheckout {
	private Git git;

	public void checkoutBranch(String branchName) throws RefAlreadyExistsException, RefNotFoundException,
			InvalidRefNameException, CheckoutConflictException, GitAPIException {
		Path branchPath = ReqsConfig.REPOSITORY_REPO;
		git = new Git((Repository) branchPath);
		git.checkout().setName(branchName).call();
	}
}
