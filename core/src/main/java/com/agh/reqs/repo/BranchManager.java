package com.agh.reqs.repo;

import java.io.IOException;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.errors.CannotDeleteCurrentBranchException;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.api.errors.DetachedHeadException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRefNameException;
import org.eclipse.jgit.api.errors.NotMergedException;
import org.eclipse.jgit.api.errors.RefAlreadyExistsException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;

public class BranchManager {

	private Repository repo;
	private Git git;

	public BranchManager(Repository repository) {
		this.repo = repository;
		git = new Git(repo);
	}

	public List<Ref> getAllBranches() throws GitAPIException, IOException {
		return git.branchList().setListMode(ListMode.ALL).call();
	}

	public void changeBranch(String branchName) throws RefAlreadyExistsException, RefNotFoundException,
			InvalidRefNameException, CheckoutConflictException, GitAPIException {
		git.checkout().setName(branchName).call();
	}

	public void removeBranch(String branchName)
			throws NotMergedException, CannotDeleteCurrentBranchException, GitAPIException {
		git.branchDelete().setBranchNames(branchName).call();
	}

	public void checkActualBranch(Repository repository) throws IOException {
		String currentBranch = repository.getBranch();
		System.out.println(currentBranch);
	}

	public void renameBranch(String newName, String oldName) throws RefNotFoundException, InvalidRefNameException,
			RefAlreadyExistsException, DetachedHeadException, GitAPIException {
		git.branchRename().setNewName(newName).setOldName(oldName).call();
	}
}
