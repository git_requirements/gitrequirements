package com.agh.reqs.project;

import java.nio.file.Paths;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.req.IdGenerator;

public class ProjectIdGenerator extends IdGenerator {

	public ProjectIdGenerator() {
		super(Paths.get(ReqsConfig.REPOSITORY_IDCOUNT_PROJECTS.toString()));
	}

	public long getId() throws Exception {
		return super.getId();
	}
}
