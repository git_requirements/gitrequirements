package com.agh.reqs.project;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.req.Req;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ProjectAdder {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	FolderCleaner FC;
	Project P;
	ProjectIdGenerator PIG;
	ProjectResource PR;
	Req projectAdd;

	public ProjectAdder() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		PIG = new ProjectIdGenerator();
		PR = new ProjectResource();
		FC = new FolderCleaner();
	}

	public void addProject() throws Exception {
		try {
			projectAdd = PR.getReqForProjectAdder();
			String name = projectAdd.getName();
			String desc = projectAdd.getDescription();
			ArrayList<String> releases = projectAdd.getReleases();
			for (String release : releases) {
				BM.changeBranch(release);
				if (!Files.exists(ReqsConfig.REPOSITORY_PROJECTS)) {
					Files.createDirectory(ReqsConfig.REPOSITORY_PROJECTS);
				}
				long id = PIG.getId();
				Path projectDescFile = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), name, "projectDesc.json");
				if (!Files.exists(projectDescFile)) {
					P = new Project(name, id, desc, releases);
					P.storeProject();
					GC.gitAddAll();
					GC.commitAll("added project " + name);
					BM.changeBranch("develop");
				}
			}
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_ADD_PROJECT.toString()));
		}
	}

}
