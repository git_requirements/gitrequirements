package com.agh.reqs.project;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqResource;
import com.agh.reqs.req.ReqWriter;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ProjectEditor {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	FolderCleaner FC;
	Project P;
	ReqWriter RW;
	ProjectResource PR;
	ReqResource RR;
	Req projectEdit, projectToEdit, simpleReqEdit;

	public ProjectEditor() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		PR = new ProjectResource();
		RR = new ReqResource();
		FC = new FolderCleaner();
		RW = new ReqWriter();
	}

	public void editProject() throws Exception {

		try {
			projectEdit = PR.getReqForProjectEditor();
			String name = projectEdit.getName();
			String oldName = projectEdit.getOldName();
			String desc = projectEdit.getDescription();
			ArrayList<String> releases = projectEdit.getReleases();

			for (String release : releases) {
				BM.changeBranch(release);
				projectToEdit = PR.getReqFromProject(oldName);
				long projId = projectToEdit.getId();
				Path newPath = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), name);
				Path oldPath = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), oldName);
				renameProject(newPath, oldPath);

				P.createFileDescAboutProj(name, projId, desc, newPath, releases);

				List<String> levels = Utils.createLevelList();

				for (String level : levels) {
					Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), name, level.toString());

					File file = new File(path.toString());
					File[] listOfFiles = file.listFiles();

					for (int i = 0; i < listOfFiles.length; i++) {
						simpleReqEdit = RR.getReqForGet(name, level, listOfFiles[i].getName());
						ArrayList<String> projects = simpleReqEdit.getProjects();
						projects.remove(oldName.toString());
						projects.add(name.toString());
						Path pathReq = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), name, level,
								listOfFiles[i].getName());

						RW.storeToFileReq(simpleReqEdit, pathReq);

					}
				}
				GC.gitAddAll();
				GC.commitAll("edited project" + name);
				BM.changeBranch("develop");
			}
		} finally {

			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_PROJECT.toString()));
		}

	}

	private void renameProject(Path newPath, Path oldPath) {
		File oldfile = new File(oldPath.toString());
		File newfile = new File(newPath.toString());

		if (oldfile.renameTo(newfile)) {
			System.out.println("Zmiana nazwy - OK");
		} else {
			System.out.println("Zmiana nazwy - NOPE");
		}
	}
}
