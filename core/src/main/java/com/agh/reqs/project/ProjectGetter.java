package com.agh.reqs.project;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.req.Req;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ProjectGetter {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	FolderCleaner FC;
	ProjectResource PR;
	Req getProjList;

	public ProjectGetter() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		PR = new ProjectResource();
		FC = new FolderCleaner();
	}

	public void getProjects() throws Exception {
		try {
			getProjList = PR.getReqFromGet();
			ArrayList<String> releases = getProjList.getReleases();
			BM.changeBranch("develop");
			Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString());
			for (String release : releases) {
				BM.changeBranch(release);
				File folder = new File(path.toString());
				File[] listOfFiles = folder.listFiles();
				ArrayList<Long> projectIdList = new ArrayList<>();
				List l1 = new LinkedList();
				Utils.collectProject(listOfFiles, projectIdList, l1);
				Utils.createListOfFile(l1, "ListOfProjectin" + release);
				GC.commitAll("take list of project on " + release + " release");
				BM.changeBranch("develop");
			}
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_PROJECT.toString()));
		}
	}
}
