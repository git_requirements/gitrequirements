package com.agh.reqs.project;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqFields;
import com.agh.reqs.util.Utils;

public class ProjectResource {
	public Req getReqForProjectAdder() throws FileNotFoundException, IOException, ParseException {
		Path pathAddProj = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_ADD_PROJECT.toString());
		return getAddProj(pathAddProj);
	}

	public Req getReqForProjectEditor() throws FileNotFoundException, IOException, ParseException {
		Path pathEditProj = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_PROJECT.toString());
		return getEditProj(pathEditProj);
	}

	public Req getReqForProjectRemove() throws FileNotFoundException, IOException, ParseException {
		Path pathRemoveProj = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_PROJECT.toString());
		return getRemoveProj(pathRemoveProj);
	}

	public Req getReqFromProject(String projectName) throws FileNotFoundException, IOException, ParseException {
		Path pathDescProj = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), projectName, "projectDesc" + ".json");
		return getAddProj(pathDescProj);
	}

	public Req getReqFromGet() throws FileNotFoundException, IOException, ParseException {
		Path pathGetProj = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_PROJECT.toString());
		return getRelease(pathGetProj);
	}

	private static Req getRelease(Path pathRelease) {
		JSONObject jsonObject = Utils.getResourceCheck(pathRelease);

		ArrayList<String> releases = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());

		Req editedReq = new Req(0);
		editedReq.setReleases(releases);

		return editedReq;
	}

	private static Req getAddProj(Path pathAddProj) {
		JSONObject jsonObject = Utils.getResourceCheck(pathAddProj);

		long id = (long) jsonObject.get(ReqFields.ID.getName());
		ArrayList<String> releases = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());
		Req editedReq = new Req(id);
		editedReq.setReleases(releases);
		getDesc(jsonObject, editedReq);
		getName(jsonObject, editedReq);

		return editedReq;
	}

	private static Req getEditProj(Path pathEditProj) {

		JSONObject jsonObject = Utils.getResourceCheck(pathEditProj);
		Req editedReq = new Req(0);

		getDesc(jsonObject, editedReq);

		getName(jsonObject, editedReq);

		String oldName = (String) jsonObject.get(ReqFields.OLDNAME.getName());
		editedReq.setOldName(oldName);
		ArrayList<String> release = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());
		editedReq.setReleases(release);
		return editedReq;
	}

	private static Req getRemoveProj(Path pathGetRemove) {
		JSONObject jsonObject = Utils.getResourceCheck(pathGetRemove);
		Req editedReq = new Req(0);

		getName(jsonObject, editedReq);

		return editedReq;
	}

	public static void getName(JSONObject jsonObject, Req editedReq) {
		String name = (String) jsonObject.get(ReqFields.NAME.getName());
		editedReq.setName(name);
	}

	public static void getDesc(JSONObject jsonObject, Req editedReq) {
		String description = (String) jsonObject.get(ReqFields.DESC.getName());
		editedReq.setDescription(description);
	}
}
