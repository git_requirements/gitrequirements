package com.agh.reqs.project;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoFilepatternException;
import org.json.simple.JSONObject;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.req.ReqFields;
import com.agh.reqs.util.Utils;

public class Project {

	private String projectName;
	private String desc;
	private ArrayList<String> release;
	private long projId;

	public Project(String projectName, long projId, String desc, ArrayList<String> release) {
		this.projectName = projectName;
		this.projId = projId;
		this.desc = desc;
		this.release = release;
	}

	public void storeProject() throws IOException, NoFilepatternException, GitAPIException {
		Path pathProject = createProjectFolder(projectName);
		createFileDescAboutProj(projectName, projId, desc, pathProject, release);
		createLevelDirs(pathProject);
	}

	private Path createProjectFolder(String projectName) throws IOException {
		Path pathProject = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), projectName);
		checkAndCreate(pathProject);
		return pathProject;
	}

	private void checkAndCreate(Path pathProjecsFolder) throws IOException {
		if (!Files.exists(pathProjecsFolder)) {
			Files.createDirectory(pathProjecsFolder);
		}
	}

	public void createFileDescAboutProj(String projectName, long projId, String desc, Path pathProject,
			ArrayList<String> release) throws IOException, NoFilepatternException, GitAPIException {
		JSONObject obj = new JSONObject();
		obj.put(ReqFields.DESC.getName(), desc);
		obj.put(ReqFields.NAME.getName(), projectName);
		obj.put(ReqFields.ID.getName(), projId);
		obj.put(ReqFields.RELEASE.getName(), release);

		try {
			Path path = Paths.get(pathProject.toString(), "projectDesc" + ".json");
			Utils.createFile(obj, path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void createLevelDirs(Path pathProject) throws IOException {
		List<String> lista = Utils.createLevelList();
		for (String a : lista) {
			Path poziomy = Paths.get(pathProject.toString(), a);
			if (Files.exists(poziomy)) {
				return;
			}
			Files.createDirectory(poziomy);
		}
	}
}
