package com.agh.reqs.project;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.req.WorkspaceReqCreator;

public class WorkspaceCreator {

	public void createWorkspace() throws IOException {
		Path workspacePath = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE.toString());
		if (Files.exists(workspacePath)) {
			System.out.println("file exist");
			return;
		}
		Files.createDirectory(workspacePath);
		Files.createDirectory(ReqsConfig.REPOSITORY_RESULTS);
		checkExistsAndCreateOptionFolderAndJsonFile(workspacePath);
		WorkspaceReqCreator WRC = new WorkspaceReqCreator();
		WRC.addOptionReq();
		WRC.addProjectOption();
		WRC.addReleaseOption();
		WRC.addTagsOption();
	}

	private void checkExistsAndCreateOptionFolderAndJsonFile(Path workspacePath) throws IOException {
		List<String> workspaceFolderList = workspaceFolderList();
		for (String a : workspaceFolderList) {
			Path pathFolderInWorkspace = Paths.get(workspacePath.toString(), a);
			if (Files.exists(pathFolderInWorkspace)) {
				System.out.println("file exist");
				return;
			}
			PutOptionFolderToFolderInWorkspace(workspacePath, a, pathFolderInWorkspace);
		}

		try {
			Path pathDeleteRemove = Paths.get(ReqsConfig.RELEASES.toString(), "remove");
			Files.delete(pathDeleteRemove);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Path jsonFile = Paths.get(ReqsConfig.REQS.toString(), "requirementsGetFromTag.json");
		if (Files.exists(jsonFile)) {
			System.out.println("file exist");
			return;
		}
		JSONObject obj = new JSONObject();
		FileWriter file = new FileWriter(jsonFile.toFile());
		file.write(obj.toJSONString());
		file.flush();
		file.close();
	}

	private void PutOptionFolderToFolderInWorkspace(Path workspacePath, String a, Path pathFolderInWorkspace)
			throws IOException {
		Files.createDirectory(pathFolderInWorkspace);
		List<String> workspaceFolderFile = workspaceFolderInFolderList();
		for (String b : workspaceFolderFile) {
			Path folder2 = Paths.get(workspacePath.toString(), a, b);
			if (Files.exists(folder2)) {
				System.out.println("file exist");
				return;
			}
			Files.createDirectory(folder2);
		}
	}

	private List<String> workspaceFolderList() {
		List<String> workspaceFolderList = new ArrayList<String>();
		workspaceFolderList.add("requirements");
		workspaceFolderList.add("releases");
		workspaceFolderList.add("projects");
		workspaceFolderList.add("tags");
		return workspaceFolderList;
	}

	private List<String> workspaceFolderInFolderList() {
		List<String> workspaceFolderFile = new ArrayList<String>();
		workspaceFolderFile.add("add");
		workspaceFolderFile.add("edit");
		workspaceFolderFile.add("get");
		workspaceFolderFile.add("remove");
		return workspaceFolderFile;
	}

}
