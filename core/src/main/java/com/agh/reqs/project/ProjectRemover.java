package com.agh.reqs.project;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.req.Req;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ProjectRemover {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	ProjectResource PR;
	FolderCleaner FC;
	Req reqToRemove, projDesc;

	public ProjectRemover() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		PR = new ProjectResource();
		FC = new FolderCleaner();
	}

	public void removeReq() throws Exception {
		try {
			reqToRemove = PR.getReqForProjectRemove();
			String projectName = reqToRemove.getName();
			Path projectPath = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), projectName);
			if (anyReqExists(projectPath)) {
				return;
			}

			projDesc = PR.getReqFromProject(projectName);
			ArrayList<String> releases = projDesc.getReleases();
			for (String release : releases) {
				BM.changeBranch(release);
				removeLevel(projectPath, Utils.createLevelList());
				removeProject(projectPath);
				GC.commitAll("project " + projectName + " deleted");
				BM.changeBranch("develop");
			}
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_PROJECT.toString()));
		}
	}

	private boolean anyReqExists(Path projectPath) {
		for (String level : Utils.createLevelList()) {
			Path levelDirectory = Paths.get(projectPath.toString(), level);
			if (Files.exists(levelDirectory) && Files.isDirectory(levelDirectory)) {
				File levelDir = new File(levelDirectory.toString());
				if (levelDir.list().length > 0) {
					System.out.println("Directory is not empty!");
					return true;
				}
			}
		}
		return false;
	}

	private void removeProject(Path path) {
		try {
			Path projDescPath = Paths.get(path.toString(), "projectDesc" + ".json");
			Files.delete(projDescPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			Files.delete(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeLevel(Path path, List<String> lista) {
		for (String a : lista) {
			Path poziomy = Paths.get(path.toString(), a);
			if (Files.exists(poziomy)) {
				try {
					Files.delete(poziomy);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
