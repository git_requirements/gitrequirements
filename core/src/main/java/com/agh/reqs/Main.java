package com.agh.reqs;

import java.nio.file.Files;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.project.ProjectAdder;
import com.agh.reqs.project.ProjectEditor;
import com.agh.reqs.project.ProjectGetter;
import com.agh.reqs.project.ProjectRemover;
import com.agh.reqs.project.WorkspaceCreator;
import com.agh.reqs.release.ReleaseAdder;
import com.agh.reqs.release.ReleaseEdit;
import com.agh.reqs.release.ReleaserGet;
import com.agh.reqs.repo.BranchCreator;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.repo.RepositoryCreator;
import com.agh.reqs.req.ReqAdder;
import com.agh.reqs.req.ReqEditor;
import com.agh.reqs.req.ReqGetter;
import com.agh.reqs.req.ReqRemover;
import com.agh.reqs.tags.TagsAdder;
import com.agh.reqs.tags.TagsEditor;
import com.agh.reqs.tags.TagsGetter;
import com.agh.reqs.tags.TagsRemover;

public class Main {

	public static void main(String[] args) throws Exception {
		System.out.println("<###################################################################################>");
		System.out.println("Aplikacja zarządzająca wymaganiami w oparciu o rozproszony system kontroli wersji GIT");
		RepositoryCreator repoCreator = new RepositoryCreator();
		Repository repository = null;
		if (!Files.exists(ReqsConfig.REPOSITORY_REPO)) {
			Files.createDirectory(ReqsConfig.REPOSITORY_REPO);
			System.out.println("path " + ReqsConfig.REPOSITORY_REPO + " created");
		}
		if (!Files.exists(ReqsConfig.REPOSITORY_RELEASES)) {
			Files.createDirectory(ReqsConfig.REPOSITORY_RELEASES);
			System.out.println("path " + ReqsConfig.REPOSITORY_RELEASES + " created");
		}
		if (!repoCreator.isRepositoryCreated()) {
			System.out.println("Repository missing... creating");
			repository = repoCreator.createEmptyRepository();
			System.out.println("repository created succesfull!");
			GitCommander gitCommander = new GitCommander(repository);
			gitCommander.commitAll("Repository born");
			BranchCreator BC = new BranchCreator(repository);
			BC.addBranch("develop");
			gitCommander.commitAll("develop branch born");
		}

		// BranchManager lrb = new BranchManager(repository);
		// List<Ref> allBranches = lrb.getAllBranches();
		// System.out.println("List of all branch");
		// for (Ref ref : allBranches) {
		// System.out.println(ref);
		// }
		if (!Files.exists(ReqsConfig.REPOSITORY_WORKSPACE)) {
			System.out.println("no workspace!");
			WorkspaceCreator WC = new WorkspaceCreator();
			WC.createWorkspace();
			System.out.println("workspace created succesfull!");
		}
		while (true) {
			try {

				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_ADD_REQ)) {
					ReqAdder RA = new ReqAdder();
					RA.addReq();
					System.out.println("Requirement added");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_REQ)) {
					ReqEditor RE = new ReqEditor();
					RE.editReq();
					System.out.println("Requirement edited");

				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQ)) {
					ReqGetter RG = new ReqGetter();
					RG.getReqs();
					System.out.println("Requirements list collected");

				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQFROMTAG)) {
					ReqGetter RG = new ReqGetter();
					RG.getReqsWithTag();
					System.out.println("Requirements list tags collected");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_REQ)) {
					ReqRemover RR = new ReqRemover();
					RR.removeReq();
					System.out.println("Requirement removed");

				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_ADD_PROJECT)) {
					ProjectAdder PA = new ProjectAdder();
					PA.addProject();
					System.out.println("Project added");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_PROJECT)) {
					ProjectEditor PE = new ProjectEditor();
					PE.editProject();
					System.out.println("Project edited");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_PROJECT)) {
					ProjectGetter PG = new ProjectGetter();
					PG.getProjects();
					System.out.println("Projects list collected");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_PROJECT)) {
					ProjectRemover PR = new ProjectRemover();
					PR.removeReq();
					System.out.println("Project removed");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_ADD_TAG)) {
					TagsAdder TA = new TagsAdder();
					TA.addTag();
					System.out.println("Tag added");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_TAG)) {
					TagsEditor TE = new TagsEditor();
					TE.editTag();
					System.out.println("Tag edited");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_TAG)) {
					TagsGetter TG = new TagsGetter();
					TG.getTags();
					System.out.println("Tags list collected");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_TAG)) {
					TagsRemover TR = new TagsRemover();
					TR.removeTag();
					System.out.println("Tag removed");
				}

				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_ADD_RELEASE)) {
					ReleaseAdder RA = new ReleaseAdder();
					RA.addRelease();
					System.out.println("Release added");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_RELEASE)) {
					ReleaseEdit RE = new ReleaseEdit();
					RE.editRelease();
					System.out.println("Release edited");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_RELEASE)) {
					ReleaserGet RG = new ReleaserGet();
					RG.getReleases();
					System.out.println("Releases list collected");
				}

				Thread.sleep(4000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
