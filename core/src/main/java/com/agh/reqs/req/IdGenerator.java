package com.agh.reqs.req;

import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class IdGenerator {

	private Path idFilePath;

	public IdGenerator(Path idFilePath) {
		this.idFilePath = idFilePath;
	}

	public long getId() throws Exception {
		createIDFileIfNotExists(idFilePath);
		long id = readIDFromFile(idFilePath);
		id++;
		storeIDToFile(id, idFilePath);
		return id;
	}

	public long readIDFromFile(Path idFilePath) throws Exception {
		JSONParser parser = new JSONParser();
		JSONObject idEntry = (JSONObject) parser.parse(new FileReader(idFilePath.toString()));
		long id = (long) idEntry.get(ReqFields.ID.getName());
		return id;
	}

	public void storeIDToFile(long id, Path idFilePath) throws Exception {
		JSONObject idEntry = new JSONObject();
		idEntry.put(ReqFields.ID.getName(), id);

		FileWriter file = new FileWriter(idFilePath.toFile());
		file.write(idEntry.toJSONString());
		file.flush();
		file.close();
	}

	public void createIDFileIfNotExists(Path idFilePath) throws Exception {
		if (Files.notExists(idFilePath)) {
			FileUtils.touch(idFilePath.toFile());
			storeIDToFile(0, idFilePath);
		}
	}
}