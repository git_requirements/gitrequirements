package com.agh.reqs.req;

public enum Level {

	L1("L1"), L2("L2"), L3("L3"), L4("L4");

	private String name;

	private Level(String name) {
		this.name = name;

	}

	public String getName() {
		return this.name;
	}
}
