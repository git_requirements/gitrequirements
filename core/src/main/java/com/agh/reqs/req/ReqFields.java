package com.agh.reqs.req;

public enum ReqFields {

	DESC("description"), PROJECT("project"), HI_LEVEL("hi_level"), LO_LEVEL("lo_level"), NAME("name"), RELEASE(
			"release"), ID("id"), LEVEL("level"), OLDNAME("old_name"), TAGS("tags"), OLDRELEASE(
					"old_release"), NEWRELEASE("new_release");

	private String name;

	private ReqFields(String name) {
		this.name = name;

	}

	public String getName() {
		return this.name;
	}
}
