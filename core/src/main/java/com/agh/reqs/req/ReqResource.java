package com.agh.reqs.req;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.project.ProjectResource;
import com.agh.reqs.util.Utils;

public class ReqResource {
	public Req getReq(String project, Level level, long id) {
		String fileName = id + ".json";
		return getReqForGet(project, level.toString(), fileName);
	}

	public Req getReqForReqEditor() throws FileNotFoundException, IOException, ParseException {
		Path pathEditReq = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_REQ.toString());
		return getFullReq(pathEditReq);
	}

	public Req getReqForReqAdder() throws FileNotFoundException, IOException, ParseException {
		Path pathAddReq = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_ADD_REQ.toString());
		return getFullReq(pathAddReq);
	}

	public Req getReqForReqRemover() throws FileNotFoundException, IOException, ParseException {
		Path pathRemoveReq = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_REQ.toString());
		return getFullRemover(pathRemoveReq);
	}

	public Req getReqForReqGetter() throws FileNotFoundException, IOException, ParseException {
		Path pathGetReq = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQ.toString());
		return getFullGetter(pathGetReq);
	}

	public Req getReqForReqGetterTags() throws FileNotFoundException, IOException, ParseException {
		Path pathGetReq = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQFROMTAG.toString());
		return getFullGetter(pathGetReq);
	}

	public Req getReqForGet(String project, String level, String str) {
		Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), project, level, str);
		return getFullReq(path);
	}

	private Req getFullReq(Path tempFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(tempFilePath);
		long id = (long) jsonObject.get(ReqFields.ID.getName());
		Level lvl = Level.valueOf(((String) jsonObject.get(ReqFields.LEVEL.getName())));
		ArrayList<String> proj = (ArrayList<String>) jsonObject.get(ReqFields.PROJECT.getName());
		ArrayList<Long> hiLevel = (ArrayList<Long>) jsonObject.get(ReqFields.HI_LEVEL.getName());
		ArrayList<Long> loLevel = (ArrayList<Long>) jsonObject.get(ReqFields.LO_LEVEL.getName());
		ArrayList<String> tags = (ArrayList<String>) jsonObject.get(ReqFields.TAGS.getName());
		ArrayList<String> releases = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());

		Req editedReq = new Req(id);
		editedReq.setLevel(lvl);
		editedReq.setProjects(proj);
		editedReq.setHiLevelReqs(hiLevel);
		editedReq.setLoLevelReqs(loLevel);
		editedReq.setTags(tags);
		editedReq.setReleases(releases);
		ProjectResource.getDesc(jsonObject, editedReq);
		ProjectResource.getName(jsonObject, editedReq);
		return editedReq;
	}

	private Req getFullRemover(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);
		long id = (long) jsonObject.get(ReqFields.ID.getName());
		Level lvl = Level.valueOf(((String) jsonObject.get(ReqFields.LEVEL.getName())));
		ArrayList<String> proj = (ArrayList<String>) jsonObject.get(ReqFields.PROJECT.getName());
		ArrayList<String> release = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());

		Req editedReq = new Req(id);
		editedReq.setLevel(lvl);
		editedReq.setProjects(proj);
		editedReq.setReleases(release);

		return editedReq;
	}

	private Req getFullGetter(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);
		Level lvl = Level.valueOf(((String) jsonObject.get(ReqFields.LEVEL.getName())));
		ArrayList<String> proj = (ArrayList<String>) jsonObject.get(ReqFields.PROJECT.getName());
		ArrayList<String> releases = (ArrayList<String>) jsonObject.get(ReqFields.RELEASE.getName());
		ArrayList<String> tags = (ArrayList<String>) jsonObject.get(ReqFields.TAGS.getName());

		Req editedReq = new Req(0);
		editedReq.setLevel(lvl);
		editedReq.setProjects(proj);
		editedReq.setReleases(releases);
		editedReq.setTags(tags);
		ProjectResource.getName(jsonObject, editedReq);

		return editedReq;
	}
}
