package com.agh.reqs.req;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

import org.json.simple.JSONObject;

import com.agh.reqs.util.Utils;

public class ReqWriter {
	public void storeToFile(Req req) throws IOException {

		JSONObject obj = new JSONObject();
		obj.put(ReqFields.DESC.getName(), getValidString(req.getDescription()));
		obj.put(ReqFields.LEVEL.getName(), req.getLevel().getName());
		obj.put(ReqFields.PROJECT.getName(), req.getProjects());
		obj.put(ReqFields.HI_LEVEL.getName(), req.getHiLevel());
		obj.put(ReqFields.LO_LEVEL.getName(), req.getLoLevel());
		obj.put(ReqFields.ID.getName(), req.getId());
		obj.put(ReqFields.NAME.getName(), req.getName());
		obj.put(ReqFields.TAGS.getName(), req.getTags());
		obj.put(ReqFields.RELEASE.getName(), req.getReleases());

		try {
			Path path = Utils.toPath(req);
			FileWriter file = new FileWriter(path.toFile());
			file.write(obj.toJSONString());
			file.flush();
			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static String getValidString(String str) {
		if (str == null) {
			return "";
		}
		return str;
	}

	public void storeToFileReq(Req req, Path path) throws IOException {

		JSONObject obj = new JSONObject();
		obj.put(ReqFields.DESC.getName(), getValidString(req.getDescription()));
		obj.put(ReqFields.LEVEL.getName(), req.getLevel().getName());
		obj.put(ReqFields.PROJECT.getName(), req.getProjects());
		obj.put(ReqFields.HI_LEVEL.getName(), req.getHiLevel());
		obj.put(ReqFields.LO_LEVEL.getName(), req.getLoLevel());
		obj.put(ReqFields.ID.getName(), req.getId());
		obj.put(ReqFields.NAME.getName(), req.getName());
		obj.put(ReqFields.TAGS.getName(), req.getTags());
		obj.put(ReqFields.RELEASE.getName(), req.getReleases());

		try {
			FileWriter file = new FileWriter(path.toFile());
			file.write(obj.toJSONString());
			file.flush();
			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
