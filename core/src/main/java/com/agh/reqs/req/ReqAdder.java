package com.agh.reqs.req;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ReqAdder {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	ReqWriter RW;
	Level level;
	ReqResource RR;
	FolderCleaner FC;
	HiLevelGetter HLG;
	ReqIdGenerator RIG;
	Req reqAdd;

	public ReqAdder() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		RR = new ReqResource();
		HLG = new HiLevelGetter();
		RIG = new ReqIdGenerator();
		FC = new FolderCleaner();
		RW = new ReqWriter();
	}

	public void addReq() throws Exception {
		try {
			reqAdd = RR.getReqForReqAdder();
			ArrayList<String> projects = reqAdd.getProjects();
			level = reqAdd.getLevel();
			ArrayList<String> releases = reqAdd.getReleases();
			String name = reqAdd.getName();
			reqAdd.setId(RIG.getId());
			for (String release : releases) {

				for (String project : projects) {
					BM.changeBranch(release);

					long id = reqAdd.getId();
					Path checkLevelPath = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), project,
							level.getName());
					if (!Files.exists(checkLevelPath)) {
						Files.createDirectory(checkLevelPath);
					}
					Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), project, level.getName(),
							id + ".json");

					if (!Files.exists(path)) {
						ArrayList<Long> hiLevelListadd = reqAdd.getHiLevel();

						if (!hiLevelListadd.isEmpty()) {
							findReqParentAndSetupLoList(id, reqAdd, project, hiLevelListadd);
						}
						RW.storeToFileReq(reqAdd, path);
						GC.gitAddAll();
						GC.commitAll("added requiremenet " + name);
						BM.changeBranch("develop");
					}

					else if (Files.exists(path)) {
						System.out.println("Nie mo�na doda� plik " + path.toString() + " ju� istnieje");
					}
				}

			}

		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_ADD_REQ.toString()));
		}
	}

	private void findReqParentAndSetupLoList(long id, Req reqAdd, String project, ArrayList<Long> hiLevelListadd)
			throws IOException {
		long parentID = hiLevelListadd.get(0);
		Level hiLevel = HLG.getHiLevel(reqAdd);
		Req reqParent = RR.getReq(project, hiLevel, parentID);

		ArrayList<Long> loLevelListParent = reqParent.getLoLevel();
		loLevelListParent.add(id);
		reqParent.setLoLevelReqs(loLevelListParent);
		RW.storeToFile(reqParent);
	}
}
