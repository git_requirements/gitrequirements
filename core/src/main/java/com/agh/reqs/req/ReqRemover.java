package com.agh.reqs.req;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ReqRemover {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	ReqWriter RW;
	ReqResource RR;
	FolderCleaner FC;
	HiLevelGetter HLG;
	Level level, hiLevel;
	Req reqPointer, reqDel;

	public ReqRemover() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		RR = new ReqResource();
		HLG = new HiLevelGetter();
		RW = new ReqWriter();
		FC = new FolderCleaner();
	}

	public void removeReq() throws Exception {
		try {
			reqPointer = RR.getReqForReqRemover();
			ArrayList<String> projects = reqPointer.getProjects();
			ArrayList<String> releases = reqPointer.getReleases();
			level = reqPointer.getLevel();
			long id = reqPointer.getId();
			for (String release : releases) {
				BM.changeBranch(release);
				for (String project : projects) {
					Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString().toString(), project,
							level.getName(), id + ".json");

					if (Files.exists(path)) {
						reqDel = RR.getReq(project, level, id);
						ArrayList<Long> loLevelListDel = reqDel.getLoLevel();
						String name = reqDel.getName();

						if (!loLevelListDel.isEmpty()) {
							FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_REQ.toString()));
							return;
						}
						ArrayList<Long> hiLevelListDel = reqDel.getHiLevel();
						hiLevel = HLG.getHiLevel(reqDel);

						if (!hiLevelListDel.isEmpty()) {
							findReqParentAndSetupLoListWhenRemove(id, reqDel, project, hiLevelListDel);
						}
						removingReq(reqDel);
						GC.gitAddAll();
						GC.commitAll("removed req " + name);
					}
				}
			}
			BM.changeBranch("develop");
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_REQ.toString()));
		}
	}

	private void removingReq(Req req) throws IOException {
		Path path = Utils.toPath(req);
		try {
			Files.delete(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void findReqParentAndSetupLoListWhenRemove(long id, Req reqAdd, String project,
			ArrayList<Long> hiLevelListadd) throws IOException {
		long parentID = hiLevelListadd.get(0);
		Level hiLevel = HLG.getHiLevel(reqAdd);
		Req reqParent = RR.getReq(project, hiLevel, parentID);

		ArrayList<Long> loLevelListParent = reqParent.getLoLevel();
		loLevelListParent.remove(id);
		reqParent.setLoLevelReqs(loLevelListParent);
		RW.storeToFile(reqParent);
	}
}
