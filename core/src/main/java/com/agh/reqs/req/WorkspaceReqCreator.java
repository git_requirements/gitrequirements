package com.agh.reqs.req;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.agh.reqs.config.ReqsConfig;

public class WorkspaceReqCreator {
	ArrayList<String> stringList = new ArrayList<>();
	ArrayList<Long> longList = new ArrayList<>();

	public void addOptionReq() {
		stringList.add("test");
		JSONObject objadd = new JSONObject();
		objadd.put(ReqFields.DESC.getName(), "test");
		objadd.put(ReqFields.LEVEL.getName(), "L1");
		objadd.put(ReqFields.PROJECT.getName(), stringList);
		objadd.put(ReqFields.HI_LEVEL.getName(), longList);
		objadd.put(ReqFields.LO_LEVEL.getName(), longList);
		objadd.put(ReqFields.ID.getName(), 0);
		objadd.put(ReqFields.NAME.getName(), "reqName");
		objadd.put(ReqFields.TAGS.getName(), stringList);
		objadd.put(ReqFields.RELEASE.getName(), stringList);
		Path path1 = Paths.get(ReqsConfig.REQS.toString(), "requirementsAdd" + ".json");
		createFile(objadd, path1);

		Path path2 = Paths.get(ReqsConfig.REQS.toString(), "requirementsEdit" + ".json");
		createFile(objadd, path2);

		JSONObject objGet = new JSONObject();
		objGet.put(ReqFields.LEVEL.getName(), "L1");
		objGet.put(ReqFields.PROJECT.getName(), stringList);
		objGet.put(ReqFields.RELEASE.getName(), stringList);
		Path path3 = Paths.get(ReqsConfig.REQS.toString(), "requirementsGet" + ".json");
		createFile(objGet, path3);

		JSONObject objRemove = new JSONObject();
		objRemove.put(ReqFields.ID.getName(), 0);
		objRemove.put(ReqFields.LEVEL.getName(), "L1");
		objRemove.put(ReqFields.PROJECT.getName(), stringList);
		objRemove.put(ReqFields.RELEASE.getName(), stringList);
		Path path4 = Paths.get(ReqsConfig.REQS.toString(), "requirementsRemove" + ".json");
		createFile(objRemove, path4);

		JSONObject objGetTag = new JSONObject();
		objGetTag.put(ReqFields.ID.getName(), 0);
		objGetTag.put(ReqFields.LEVEL.getName(), "L1");
		objGetTag.put(ReqFields.PROJECT.getName(), stringList);
		objGetTag.put(ReqFields.RELEASE.getName(), stringList);
		objGetTag.put(ReqFields.TAGS.getName(), stringList);
		Path path5 = Paths.get(ReqsConfig.REQS.toString(), "requirementsGetFromTag" + ".json");
		createFile(objGetTag, path5);

	}

	public void addProjectOption() {
		JSONObject objadd = new JSONObject();
		objadd.put(ReqFields.DESC.getName(), "test");
		objadd.put(ReqFields.ID.getName(), 0);
		objadd.put(ReqFields.NAME.getName(), "reqName");
		objadd.put(ReqFields.RELEASE.getName(), stringList);
		Path path1 = Paths.get(ReqsConfig.PROJECTS.toString(), "projectsAdd" + ".json");
		createFile(objadd, path1);

		JSONObject objEdit = new JSONObject();
		objEdit.put(ReqFields.DESC.getName(), "test");
		objEdit.put(ReqFields.NAME.getName(), "newname");
		objEdit.put(ReqFields.OLDNAME.getName(), "oldname");
		objEdit.put(ReqFields.RELEASE.getName(), stringList);
		Path path2 = Paths.get(ReqsConfig.PROJECTS.toString(), "projectsEdit" + ".json");
		createFile(objEdit, path2);

		JSONObject objGet = new JSONObject();
		objGet.put(ReqFields.RELEASE.getName(), stringList);
		Path path3 = Paths.get(ReqsConfig.PROJECTS.toString(), "projectsGet" + ".json");
		createFile(objGet, path3);

		JSONObject objRemove = new JSONObject();
		objRemove.put(ReqFields.NAME.getName(), "name");
		Path path4 = Paths.get(ReqsConfig.PROJECTS.toString(), "projectsRemove" + ".json");
		createFile(objRemove, path4);
	}

	public void addTagsOption() {
		JSONObject objadd = new JSONObject();
		objadd.put(ReqFields.ID.getName(), 0);
		objadd.put(ReqFields.NAME.getName(), "tagName");
		objadd.put(ReqFields.RELEASE.getName(), stringList);
		Path path1 = Paths.get(ReqsConfig.TAGS.toString(), "tagsAdd" + ".json");
		createFile(objadd, path1);

		JSONObject objedit = new JSONObject();
		objedit.put(ReqFields.NAME.getName(), "tagName");
		objedit.put(ReqFields.OLDNAME.getName(), "tagOldName");
		objedit.put(ReqFields.RELEASE.getName(), stringList);
		Path path2 = Paths.get(ReqsConfig.TAGS.toString(), "tagsEdit" + ".json");
		createFile(objedit, path2);

		JSONObject objGet = new JSONObject();
		objGet.put(ReqFields.RELEASE.getName(), stringList);
		Path path3 = Paths.get(ReqsConfig.TAGS.toString(), "tagsGet" + ".json");
		createFile(objGet, path3);

		JSONObject objRemove = new JSONObject();
		objRemove.put(ReqFields.RELEASE.getName(), stringList);
		objRemove.put(ReqFields.NAME.getName(), "tagName");
		Path path4 = Paths.get(ReqsConfig.TAGS.toString(), "tagsRemove" + ".json");
		createFile(objRemove, path4);
	}

	public void addReleaseOption() {
		JSONObject objadd = new JSONObject();
		objadd.put(ReqFields.DESC.getName(), "test");
		objadd.put(ReqFields.ID.getName(), 0);
		objadd.put(ReqFields.NAME.getName(), "releasename");
		Path path1 = Paths.get(ReqsConfig.RELEASES.toString(), "releasesAdd" + ".json");
		createFile(objadd, path1);

		JSONObject objEdit = new JSONObject();
		objEdit.put(ReqFields.DESC.getName(), "test");
		objEdit.put(ReqFields.ID.getName(), 0);
		objEdit.put(ReqFields.OLDNAME.getName(), "oldReleaseName");
		objEdit.put(ReqFields.NAME.getName(), "releasename");
		Path path2 = Paths.get(ReqsConfig.RELEASES.toString(), "releasesEdit" + ".json");
		createFile(objEdit, path2);

		JSONObject objGet = new JSONObject();
		Path path3 = Paths.get(ReqsConfig.RELEASES.toString(), "releasesGet" + ".json");
		createFile(objGet, path3);
	}

	private void createFile(JSONObject objadd, Path path) {
		try {

			FileWriter file = new FileWriter(path.toFile());
			file.write(objadd.toJSONString());
			file.flush();
			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
