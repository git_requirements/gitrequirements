package com.agh.reqs.req;

import java.nio.file.Paths;

import com.agh.reqs.config.ReqsConfig;

public class ReqIdGenerator extends IdGenerator {

	public ReqIdGenerator() {
		super(Paths.get(ReqsConfig.REPOSITORY_IDCOUNT_TAGS.toString()));
	}

	public long getId() throws Exception {
		return super.getId();
	}
}