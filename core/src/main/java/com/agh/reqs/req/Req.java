package com.agh.reqs.req;

import java.nio.file.Path;
import java.util.ArrayList;

public class Req {
	long id;
	String desc;
	String name;
	String oldName;
	Level level;
	Path reqPath;
	Req reqHiLevel;
	String release;
	ArrayList<String> project;
	ArrayList<String> tags;
	ArrayList<Long> hiLevelReqs;
	ArrayList<Long> loLevelReqs;
	ArrayList<String> releases;
	String oldRelease;
	String newRelease;

	public Req(long id) {
		this.id = id;
		this.release = release;
		hiLevelReqs = new ArrayList<>();
		loLevelReqs = new ArrayList<>();
		project = new ArrayList<>();
		tags = new ArrayList<>();
		releases = new ArrayList<>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDescription(String a_desc) {
		desc = a_desc;
	}

	public String getDescription() {
		return desc;
	}

	public void setLevel(Level lvl) {
		this.level = lvl;
	}

	public Level getLevel() {
		return level;
	}

	public void addTags(String tags) {
		this.tags.add(tags);
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public void addProject(String project) {
		this.project.add(project);
	}

	public ArrayList<String> getProjects() {
		return project;
	}

	public void setProjects(ArrayList<String> project) {
		this.project = project;
	}

	public void addReleases(String releases) {
		this.releases.add(releases);
	}

	public ArrayList<String> getReleases() {
		return releases;
	}

	public void setReleases(ArrayList<String> releases) {
		this.releases = releases;
	}

	public void setRelease(String release) {
		this.release = release;
	}

	public String getRelease() {
		return this.release;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOldName() {
		return this.oldName;
	}

	public void setOldName(String oldName) {
		this.oldName = oldName;
	}

	public void setNewRelease(String newRelease) {
		this.newRelease = newRelease;
	}

	public String getNewRelease() {
		return this.newRelease;
	}

	public void setOldRelease(String oldRelease) {
		this.oldRelease = oldRelease;
	}

	public String getOldRelease() {
		return this.oldRelease;
	}

	public void addLoLevel(Long loLevelId) {
		this.loLevelReqs.add(loLevelId);
	}

	public ArrayList<Long> getLoLevel() {
		return this.loLevelReqs;
	}

	public void addHiLevel(Long hiLevelId) {
		this.hiLevelReqs.add(hiLevelId);
	}

	public ArrayList<Long> getHiLevel() {
		return this.hiLevelReqs;
	}

	public void setHiLevelReqs(ArrayList<Long> hiLevel) {
		this.hiLevelReqs = hiLevel;
	}

	public void setLoLevelReqs(ArrayList<Long> loLevel) {
		this.loLevelReqs = loLevel;
	}

}