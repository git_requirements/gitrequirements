package com.agh.reqs.req;

public class HiLevelGetter {
	public Level getHiLevel(Req loLevelReq) {
		Level hiLevel = Level.L1;
		if (loLevelReq.getLevel() == Level.L2) {
			hiLevel = Level.L1;

		}
		if (loLevelReq.getLevel() == Level.L3) {
			hiLevel = Level.L2;
		}
		if (loLevelReq.getLevel() == Level.L4) {
			hiLevel = Level.L3;
		}
		return hiLevel;
	};
}
