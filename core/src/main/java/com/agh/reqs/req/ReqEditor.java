package com.agh.reqs.req;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.eclipse.jgit.lib.Repository;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.repo.GitCommander;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ReqEditor {
	Repository repository;
	BranchManager BM;
	GitCommander GC;
	FolderCleaner FC;
	ReqWriter RW;
	Level level;
	ReqResource RR;
	Req editReq, reqToEdit;

	public ReqEditor() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		GC = new GitCommander(repository);
		RR = new ReqResource();
		FC = new FolderCleaner();
		RW = new ReqWriter();
	}

	public void editReq() throws Exception {
		try {
			editReq = RR.getReqForReqEditor();
			long id = editReq.getId();
			String name = editReq.getName();
			level = editReq.getLevel();
			ArrayList<String> projects = editReq.getProjects();
			ArrayList<String> releases = editReq.getReleases();
			ArrayList<String> tags = editReq.getTags();
			BM.changeBranch("develop");
			for (String release : releases) {
				BM.changeBranch(release);
				for (String project : projects) {
					Path path = Paths.get(ReqsConfig.REPOSITORY_PROJECTS.toString(), project, level.getName(),
							id + ".json");
					if (Files.exists(path)) {
						reqToEdit = RR.getReq(project, level, id);
						ArrayList<Long> loLevel = reqToEdit.getLoLevel();
						ArrayList<Long> hiLevel = reqToEdit.getHiLevel();
						ArrayList<String> oldtags = reqToEdit.getTags();
						if (tags.isEmpty()) {
							editReq.setTags(oldtags);
						}
						editReq.setHiLevelReqs(hiLevel);
						editReq.setLoLevelReqs(loLevel);
						RW.storeToFile(editReq);
						GC.gitAddAll();
						GC.commitAll("edited Req " + name);
						BM.changeBranch("develop");
					}
				}
			}
			BM.changeBranch("develop");
		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_REQ.toString()));
		}
	}

}
