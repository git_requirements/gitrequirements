package com.agh.reqs;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.json.simple.JSONObject;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.release.ReleaseIdGenerator;
import com.agh.reqs.req.ReqFields;

public class JframeRelease extends JFrame implements ActionListener {

	JButton bdata, bwyjscie;
	JLabel lwyswietl, lnazwa, ldesc;
	JTextField a, b, c;

	public JframeRelease() {
		setSize(400, 200);
		setLocationRelativeTo(null);
		setTitle("Add Release");
		setLayout(null);
		bdata = new JButton("dodaj");
		bdata.setBounds(1, 1, 100, 20);
		add(bdata);
		bdata.addActionListener(this);

		bwyjscie = new JButton("wyjscie");
		bwyjscie.setBounds(1, 22, 100, 20);
		add(bwyjscie);
		bwyjscie.addActionListener(this);
		lnazwa = new JLabel("podaj nazwe");
		lnazwa.setBounds(253, 1, 150, 20);
		add(lnazwa);
		ldesc = new JLabel("podaj opis");
		ldesc.setBounds(253, 21, 150, 20);
		add(ldesc);
		lwyswietl = new JLabel();
		lwyswietl.setBounds(100, 50, 200, 20);
		lwyswietl.setForeground(Color.GREEN);
		lwyswietl.setFont(new Font("lololol", Font.BOLD, 20));
		add(lwyswietl);

		a = new JTextField();
		a.setBounds(102, 1, 150, 20);
		add(a);
		a.addActionListener(this);

		b = new JTextField();
		b.setBounds(102, 22, 150, 20);
		add(b);
		b.addActionListener(this);

		c = new JTextField();
		c.setBounds(102, 1, 150, 20);
		add(c);
		c.addActionListener(this);
	}

	public static void main(String[] args) {
		JframeRelease ps = new JframeRelease();
		ps.setDefaultCloseOperation(EXIT_ON_CLOSE);
		ps.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		String desc;
		String name;
		if (source == bdata) {
			desc = b.getText();
			name = a.getText();
			ReleaseIdGenerator RIG = new ReleaseIdGenerator();
			JSONObject objadd = new JSONObject();
			objadd.put(ReqFields.DESC.getName(), desc);
			try {
				objadd.put(ReqFields.ID.getName(), RIG.getId());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			objadd.put(ReqFields.NAME.getName(), name);
			Path path = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_ADD_RELEASE.toString());
			if (Files.exists(path)) {
				lwyswietl.setText("PLIK JU� ISTNIEJE");
			} else if (!Files.exists(path)) {
				try {

					FileWriter file = new FileWriter(path.toFile());
					file.write(objadd.toJSONString());
					file.flush();
					file.close();
					lwyswietl.setText("PLIK UTWORZONY");

				} catch (IOException example) {
					example.printStackTrace();
				}
			}
		}
		if (source == bwyjscie) {
			dispose();
		}
	}

}
