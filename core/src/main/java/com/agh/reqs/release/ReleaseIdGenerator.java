package com.agh.reqs.release;

import java.nio.file.Paths;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.req.IdGenerator;

public class ReleaseIdGenerator extends IdGenerator {

	public ReleaseIdGenerator() {
		super(Paths.get(ReqsConfig.REPOSITORY_IDCOUNT_RELEASE.toString()));
	}

	public long getId() throws Exception {
		return super.getId();
	}
}
