package com.agh.reqs.release;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.project.ProjectResource;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqFields;
import com.agh.reqs.util.Utils;

public class ReleaseResource {
	public Req getReqForReleaseAdder() throws FileNotFoundException, IOException, ParseException {
		Path pathAddTag = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_ADD_RELEASE.toString());
		return getIdDescName(pathAddTag);
	}

	public Req getReqForReleaseEditor() throws FileNotFoundException, IOException, ParseException {
		Path pathAddTag = Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_RELEASE.toString());
		return getIdDescNameOldname(pathAddTag);
	}

	public Req getReleaseToEdit(String releaseName) throws FileNotFoundException, IOException, ParseException {
		Path getRelease = Paths.get(ReqsConfig.RELEASES_PATH.toString(), releaseName + ".json");
		return getIdAndNameSpec(getRelease);
	}

	public Req getReleaseforReleaseGetter(String releaseName)
			throws FileNotFoundException, IOException, ParseException {
		Path addFilePath = Paths.get(ReqsConfig.REPOSITORY_RELEASES.toString(), releaseName + ".json");
		return getIdAndName(addFilePath);
	}

	private Req getIdDescName(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);
		long id = (long) jsonObject.get(ReqFields.ID.getName());

		Req editedReq = new Req(id);
		ProjectResource.getDesc(jsonObject, editedReq);
		ProjectResource.getName(jsonObject, editedReq);

		return editedReq;
	}

	private Req getIdDescNameOldname(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);
		long id = (long) jsonObject.get(ReqFields.ID.getName());
		String oldname = (String) jsonObject.get(ReqFields.OLDNAME.getName());

		Req editedReq = new Req(id);
		editedReq.setOldName(oldname);
		ProjectResource.getDesc(jsonObject, editedReq);
		ProjectResource.getName(jsonObject, editedReq);
		return editedReq;
	}

	private Req getIdAndNameSpec(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);
		long id = (long) jsonObject.get(ReqFields.ID.getName());
		Req editedReq = new Req(id);
		ProjectResource.getDesc(jsonObject, editedReq);
		ProjectResource.getName(jsonObject, editedReq);

		return editedReq;
	}

	private Req getIdAndName(Path addFilePath) {
		JSONObject jsonObject = Utils.getResourceCheck(addFilePath);
		long id = (long) jsonObject.get(ReqFields.ID.getName());
		Req editedReq = new Req(id);
		ProjectResource.getName(jsonObject, editedReq);

		return editedReq;
	}

}
