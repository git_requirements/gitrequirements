package com.agh.reqs.release;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.jgit.lib.Repository;
import org.json.simple.JSONObject;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchCreator;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqFields;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ReleaseAdder {
	Repository repository;
	BranchCreator BC;
	BranchManager BM;
	FolderCleaner FC;
	ReleaseResource RR;
	ReleaseIdGenerator RIG;
	Req releaseAdd;

	public ReleaseAdder() throws IOException {
		repository = Utils.takeThisRepo();
		BC = new BranchCreator(repository);
		BM = new BranchManager(repository);
		RR = new ReleaseResource();
		RIG = new ReleaseIdGenerator();
		FC = new FolderCleaner();
	}

	public void addRelease() throws Exception {
		try {
			releaseAdd = RR.getReqForReleaseAdder();
			long id = RIG.getId();
			releaseAdd.setId(id);
			String name = releaseAdd.getName();
			String desc = releaseAdd.getDescription();
			if (Files.exists(Paths.get(ReqsConfig.REPOSITORY_RELEASES.toString()))) {
				createRelease(id, name, desc);

				BM.changeBranch("develop");
				BC.addBranch(name);
			}
		} finally {

			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_ADD_RELEASE.toString()));
		}
	}

	public void createRelease(long id, String name, String desc) {
		Path pathTag = Paths.get(ReqsConfig.REPOSITORY_RELEASES.toString());
		JSONObject obj = new JSONObject();
		obj.put(ReqFields.NAME.getName(), name);
		obj.put(ReqFields.ID.getName(), id);
		obj.put(ReqFields.DESC.getName(), desc);

		createReleaseFile(name, pathTag, obj);
	}

	private void createReleaseFile(String name, Path pathTag, JSONObject obj) {
		try {
			Path path = Paths.get(pathTag.toString(), name + ".json");
			if (!Files.exists(path)) {
				FileWriter file = new FileWriter(path.toFile());
				file.write(obj.toJSONString());
				file.flush();
				file.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
