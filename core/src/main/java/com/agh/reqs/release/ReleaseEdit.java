package com.agh.reqs.release;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.jgit.api.errors.DetachedHeadException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRefNameException;
import org.eclipse.jgit.api.errors.RefAlreadyExistsException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.lib.Repository;
import org.json.simple.parser.ParseException;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.repo.BranchManager;
import com.agh.reqs.req.Req;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ReleaseEdit {
	BranchManager BM;
	ReleaseAdder RA;
	Repository repository;
	FolderCleaner FC;
	ReleaseResource RR;
	Req releaseEdit, releaseToEdit;

	public ReleaseEdit() throws IOException {
		repository = Utils.takeThisRepo();
		BM = new BranchManager(repository);
		RA = new ReleaseAdder();
		RR = new ReleaseResource();
		FC = new FolderCleaner();
	}

	public void editRelease() throws FileNotFoundException, IOException, ParseException, RefNotFoundException,
			InvalidRefNameException, RefAlreadyExistsException, DetachedHeadException, GitAPIException {
		try {
			releaseEdit = RR.getReqForReleaseEditor();

			String name = releaseEdit.getName();
			String oldName = releaseEdit.getOldName();
			String desc = releaseEdit.getDescription();

			releaseToEdit = RR.getReleaseToEdit(oldName);
			long releaseId = releaseToEdit.getId();

			removeRelease(oldName);
			RA.createRelease(releaseId, name, desc);
			BM.changeBranch("develop");
			BM.renameBranch(name, oldName);

		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_RELEASE.toString()));
		}
	}

	private void removeRelease(String oldName) {
		try {
			Path pathRelease = Paths.get(ReqsConfig.REPOSITORY_RELEASES.toString());
			Path pathReleaseToRemove = Paths.get(pathRelease.toString(), oldName + ".json");
			if (Files.exists(pathReleaseToRemove)) {
				Files.delete(pathReleaseToRemove);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
