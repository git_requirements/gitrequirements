package com.agh.reqs.release;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ParseException;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.req.Req;
import com.agh.reqs.req.ReqFields;
import com.agh.reqs.util.FolderCleaner;
import com.agh.reqs.util.Utils;

public class ReleaserGet {
	FolderCleaner FC;
	ReleaseResource RR;

	public ReleaserGet() throws IOException {
		FC = new FolderCleaner();
		RR = new ReleaseResource();
	}

	public void getReleases() throws FileNotFoundException, IOException, ParseException {
		try {
			Path path = Paths.get(ReqsConfig.REPOSITORY_RELEASES.toString());

			File folder = new File(path.toString());
			File[] listOfFiles = folder.listFiles();
			ArrayList<Long> lista = new ArrayList<>();
			List l1 = new LinkedList();

			collectReleases(listOfFiles, lista, l1);

			Utils.createListOfFile(l1, "ListOfReleases");

		} finally {
			FC.cleanFolderReq(Paths.get(ReqsConfig.REPOSITORY_WORKSPACE_GET_RELEASE.toString()));
		}
	}

	private void collectReleases(File[] listOfFiles, ArrayList<Long> lista, List l1)
			throws FileNotFoundException, IOException, ParseException {
		for (int i = 0; i < listOfFiles.length; i++) {
			Req req = RR.getReleaseforReleaseGetter(listOfFiles[i].getName());
			long id = req.getId();
			lista.add(id);
			Map a = new LinkedHashMap();

			a.put(ReqFields.ID.getName(), req.getId());
			a.put(ReqFields.NAME.getName(), req.getName());
			l1.add(a);
		}
	}
}
