package com.agh.reqs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Files;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import com.agh.reqs.config.ReqsConfig;
import com.agh.reqs.project.ProjectAdder;
import com.agh.reqs.project.ProjectEditor;
import com.agh.reqs.project.ProjectGetter;
import com.agh.reqs.project.ProjectRemover;
import com.agh.reqs.release.ReleaseAdder;
import com.agh.reqs.release.ReleaseEdit;
import com.agh.reqs.release.ReleaserGet;
import com.agh.reqs.req.ReqAdder;
import com.agh.reqs.req.ReqEditor;
import com.agh.reqs.req.ReqGetter;
import com.agh.reqs.req.ReqRemover;
import com.agh.reqs.tags.TagsAdder;
import com.agh.reqs.tags.TagsEditor;
import com.agh.reqs.tags.TagsGetter;
import com.agh.reqs.tags.TagsRemover;

public class menu extends JFrame implements ActionListener {

	JMenuBar menuBar;
	JMenu menuPlik, menuNarzedzia, menuOpcje;
	JMenuItem mPomoc, mWyjscie, mReq, mProj, mTag, mRel;

	public menu() {
		setTitle("aplikacja");
		setSize(400, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setLocationRelativeTo(null);
		menuBar = new JMenuBar();
		menuPlik = new JMenu("Plik");
		mReq = new JMenuItem("requirements");
		mProj = new JMenuItem("projects");
		mTag = new JMenuItem("tags");
		mRel = new JMenuItem("releases");
		mRel.addActionListener(this);
		menuPlik.add(mReq);
		menuPlik.add(mProj);
		menuPlik.add(mTag);
		menuPlik.add(mRel);
		menuNarzedzia = new JMenu("narzedzia");
		menuOpcje = new JMenu("Opcje");
		mWyjscie = new JMenuItem("wyjscie");
		mPomoc = new JMenuItem("pomoc");
		menuOpcje.add(mPomoc);
		mPomoc.addActionListener(this);
		menuOpcje.add(mWyjscie);
		mWyjscie.addActionListener(this);
		setJMenuBar(menuBar);
		menuBar.add(menuPlik);
		menuBar.add(menuNarzedzia);
		menuBar.add(menuOpcje);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {
		Object z = e.getSource();
		if (z == mRel) {
			new JframeRelease().show();
		}

		if (z == mWyjscie) {
			dispose();
		}
		if (z == mPomoc) {
			JOptionPane.showMessageDialog(null,
					"aplikacja zarządzająca wymaganiami w oparciu o rozszerzoną kontrolę wersji");
		}

	}

	public static void main(String[] args) {
		menu appmenu = new menu();
		appmenu.setVisible(true);
		while (true) {
			try {

				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_ADD_REQ)) {
					ReqAdder RA = new ReqAdder();
					RA.addReq();
					System.out.println("Requirement added");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_REQ)) {
					ReqEditor RE = new ReqEditor();
					RE.editReq();
					System.out.println("Requirement edited");

				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQ)) {
					ReqGetter RG = new ReqGetter();
					RG.getReqs();
					System.out.println("Requirements list collected");

				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_REQFROMTAG)) {
					ReqGetter RG = new ReqGetter();
					RG.getReqsWithTag();
					System.out.println("Requirements list tags collected");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_REQ)) {
					ReqRemover RR = new ReqRemover();
					RR.removeReq();
					System.out.println("Requirement removed");

				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_ADD_PROJECT)) {
					ProjectAdder PA = new ProjectAdder();
					PA.addProject();
					System.out.println("Project added");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_PROJECT)) {
					ProjectEditor PE = new ProjectEditor();
					PE.editProject();
					System.out.println("Project edited");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_PROJECT)) {
					ProjectGetter PG = new ProjectGetter();
					PG.getProjects();
					System.out.println("Projects list collected");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_PROJECT)) {
					ProjectRemover PR = new ProjectRemover();
					PR.removeReq();
					System.out.println("Project removed");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_ADD_TAG)) {
					TagsAdder TA = new TagsAdder();
					TA.addTag();
					System.out.println("Tag added");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_TAG)) {
					TagsEditor TE = new TagsEditor();
					TE.editTag();
					System.out.println("Tag edited");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_TAG)) {
					TagsGetter TG = new TagsGetter();
					TG.getTags();
					System.out.println("Tags list collected");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_REMOVE_TAG)) {
					TagsRemover TR = new TagsRemover();
					TR.removeTag();
					System.out.println("Tag removed");
				}

				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_ADD_RELEASE)) {
					ReleaseAdder RA = new ReleaseAdder();
					RA.addRelease();
					System.out.println("Release added");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_EDIT_RELEASE)) {
					ReleaseEdit RE = new ReleaseEdit();
					RE.editRelease();
					System.out.println("Release edited");
				}
				if (Files.exists(ReqsConfig.REPOSITORY_WORKSPACE_GET_RELEASE)) {
					ReleaserGet RG = new ReleaserGet();
					RG.getReleases();
					System.out.println("Releases list collected");
				}

				Thread.sleep(4000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
