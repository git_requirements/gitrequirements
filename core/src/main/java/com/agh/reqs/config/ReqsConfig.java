package com.agh.reqs.config;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ReqsConfig {
	private static String linuxPath = "/tmp";
	private static String windowsPath = "D:";
	private static String path = windowsPath;

	public static Path REPOSITORY_REPO = Paths.get(path, "repository");
	public static Path REPOSITORY_IDCOUNT_REQ = Paths.get(REPOSITORY_REPO.toString(), "idReq.json");
	public static Path REPOSITORY_IDCOUNT_TAGS = Paths.get(REPOSITORY_REPO.toString(), "idTags.json");
	public static Path REPOSITORY_IDCOUNT_PROJECTS = Paths.get(REPOSITORY_REPO.toString(), "idProjects.json");
	public static Path REPOSITORY_IDCOUNT_RELEASE = Paths.get(REPOSITORY_REPO.toString(), "idRelease.json");

	public static Path STORAGE_PATH = Paths.get(REPOSITORY_REPO.toString(), "storage");
	public static Path GIT_PATH = Paths.get(STORAGE_PATH.toString(), ".git");

	public static Path RELEASES_PATH = Paths.get(REPOSITORY_REPO.toString(), "releases");
	public static Path REPOSITORY_WORKSPACE = Paths.get(REPOSITORY_REPO.toString(), "workspace");
	public static Path REPOSITORY_RESULTS = Paths.get(REPOSITORY_WORKSPACE.toString(), "results");

	public static Path REPOSITORY_PROJECTS = Paths.get(STORAGE_PATH.toString(), "projects");
	public static Path REPOSITORY_TAGS = Paths.get(STORAGE_PATH.toString(), "tags");
	public static Path REPOSITORY_REQS = Paths.get(STORAGE_PATH.toString(), "requirements");
	public static Path REPOSITORY_RELEASES = Paths.get(REPOSITORY_REPO.toString(), "releases");

	public static Path REQS = Paths.get(REPOSITORY_WORKSPACE.toString(), "requirements");
	public static Path REPOSITORY_WORKSPACE_ADD_REQ = Paths.get(REQS.toString(), "add", "requirementsAdd" + ".json");
	public static Path REPOSITORY_WORKSPACE_EDIT_REQ = Paths.get(REQS.toString(), "edit", "requirementsEdit" + ".json");
	public static Path REPOSITORY_WORKSPACE_GET_REQ = Paths.get(REQS.toString(), "get", "requirementsGet" + ".json");
	public static Path REPOSITORY_WORKSPACE_GET_REQFROMTAG = Paths.get(REQS.toString(), "get",
			"requirementsGetFromTag" + ".json");
	public static Path REPOSITORY_WORKSPACE_REMOVE_REQ = Paths.get(REQS.toString(), "remove",
			"requirementsRemove" + ".json");

	public static Path PROJECTS = Paths.get(REPOSITORY_WORKSPACE.toString(), "projects");
	public static Path REPOSITORY_WORKSPACE_ADD_PROJECT = Paths.get(PROJECTS.toString(), "add",
			"projectsAdd" + ".json");
	public static Path REPOSITORY_WORKSPACE_EDIT_PROJECT = Paths.get(PROJECTS.toString(), "edit",
			"projectsEdit" + ".json");
	public static Path REPOSITORY_WORKSPACE_GET_PROJECT = Paths.get(PROJECTS.toString(), "get",
			"projectsGet" + ".json");
	public static Path REPOSITORY_WORKSPACE_REMOVE_PROJECT = Paths.get(PROJECTS.toString(), "remove",
			"projectsRemove" + ".json");

	public static Path TAGS = Paths.get(REPOSITORY_WORKSPACE.toString(), "tags");
	public static Path REPOSITORY_WORKSPACE_ADD_TAG = Paths.get(TAGS.toString(), "add", "tagsAdd" + ".json");
	public static Path REPOSITORY_WORKSPACE_EDIT_TAG = Paths.get(TAGS.toString(), "edit", "tagsEdit" + ".json");
	public static Path REPOSITORY_WORKSPACE_GET_TAG = Paths.get(TAGS.toString(), "get", "tagsGet" + ".json");
	public static Path REPOSITORY_WORKSPACE_REMOVE_TAG = Paths.get(TAGS.toString(), "remove", "tagsRemove" + ".json");

	public static Path RELEASES = Paths.get(REPOSITORY_WORKSPACE.toString(), "releases");
	public static Path REPOSITORY_WORKSPACE_ADD_RELEASE = Paths.get(RELEASES.toString(), "add",
			"releasesAdd" + ".json");
	public static Path REPOSITORY_WORKSPACE_EDIT_RELEASE = Paths.get(RELEASES.toString(), "edit",
			"releasesEdit" + ".json");
	public static Path REPOSITORY_WORKSPACE_GET_RELEASE = Paths.get(RELEASES.toString(), "get",
			"releasesGet" + ".json");
	public static Path REPOSITORY_WORKSPACE_REMOVE_RELEASE = Paths.get(RELEASES.toString(), "remove",
			"releasesRemove" + ".json");
	// TODO add all required paths
}
