### Git

Proponowany [Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow):

1. Podstawowa gałąź to *master*.
2. Tworzona jest pojedyncza gałąź *develop*.
3. Do *mastera* mergowane są tylko kolejne większe wersje aplikacji.
4. Na każdy feature tworzona powinna być nowa gałąź:
Dla UI: *feature/ui/nazwa_funkcjonalności*
Dla core: *feature/core/nazwa_funkcjonalności*
Gałązki odciągamy z gałęzi develop.
5. Po skończonej pracy nad daną funkcjonalnością tworzony jest *Pull Request*. Tworzymy go poprzez GUI Bitbucketa -> Pull Requests -> Create Pull Request. Wybieramy gałąź z której chcemy mergować. Jako cel mergowania wybieramy gałąź *develop*.
6. Do *Pull Requesta* dodajemy innych developerów jako recenzentów.
7. Po zaakceptowaniu zmian przez wszystkich recenzentów możliwe jest zmergowanie kodu do *develop*.

