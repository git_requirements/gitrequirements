(function() {
  "user strict"
  angular.module("gitReqs").directive('userInfo', [ userInfo]);

  function userInfo(){
      return {
        templateUrl: "app/workspace/user-info.html" ,
        restrict: "E",
        scope : {
          user : '='
        },
        replace: true
      }
    }
})();
