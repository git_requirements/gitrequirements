r1diff = [{
    "type": "add",
    "content": "(function() {"
  }, {
    "type": "add",
    "content": "'use strict'"
  }, {
    "type": "rem",
    "content": "  angular.module('gitReqs').controller('RequirementFrameController', [RequirementDetailsController, RequirementDetailsController, RequirementDetailsController]);"
  }, {
    "type": "add",
    "content": ""
  }, {
    "type": "rem",
    "content": "  function RequirementDetailsController() {"
  }

  , {
    "type": "org",
    "content": "var vm = this;"
  }, {
    "type": "org",
    "content": "  vm.collapsed = (vm.initialCollapsed === 'true')"
  }, {
    "type": "org",
    "content": "})();"
  }
]

r2diff = [{
    "type": "add",
    "content": "(function() {"
  }, {
    "type": "add",
    "content": "'use strict'"
  }, {
    "type": "rem",
    "content": "  angular.module('gitReqs').controller('AnotherController', [AnotherController]);"
  }, {
    "type": "add",
    "content": ""
  }, {
    "type": "rem",
    "content": "  function AnotherController() {"
  }

  , {
    "type": "org",
    "content": "var vm = this;"
  }, {
    "type": "org",
    "content": "vm.test='Hello';"
  }, {
    "type": "add",
    "content": "vm.hello = 'test';"
  }, {
    "type": "rem",
    "content": "var vm = this;"
  }

  , {
    "type": "org",
    "content": "  vm.collapsed = (vm.initialCollapsed === 'true')"
  }, {
    "type": "org",
    "content": "})();"
  }
]


r1HighLevel = [{
  "type": "add",
  "id": "1",
  "title": "Title1"
}]

r1LowLevel = [{
  "type": "add",
  "id": "2",
  "title": "Title2"
}, {
  "type": "org",
  "id": "1",
  "title": "Title12"
}]

r1Test = [{
  "type": "org",
  "id": "2",
  "title": "Test1"
}]

r1system = [{
  "type": "rem",
  "id": "1",
  "title": "Astro"
}]


r1 = {
  "diff": {
    "desc": r1diff,
    "lowLevel": r1LowLevel,
    "highLevel": r1HighLevel,
    "test": r1Test,
    "system": r1system
  },
  "info": {
    "id": 1,
    "title": "Login Page Behavior",
    "added": 3,
    "removed": 2
  }
};
r2 = {
  "diff": {
    "desc": r2diff
  },
  "info": {
    "id": 2,
    "title": "Lorem Ipsum",
    "added": 4,
    "removed": 0
  }
};

r3 = {
  "diff": {
    "desc": r2diff
  },
  "info": {
    "id": 3,
    "title": "Buenos Diaz Mr. Siarra",
    "added": 0,
    "removed": 3
  }
};

r4 = {
  "diff": {
    "desc": r2diff
  },
  "info": {
    "id": 4,
    "title": "Bueanos Aires",
    "added": 411,
    "removed": 220
  }
};
