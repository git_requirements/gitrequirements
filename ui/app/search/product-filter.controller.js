(function() {
    "use strict"
    angular.module("gitReqs").controller('ProductFilterController', ['filterService', '$scope', ProductFilterController]);

    function ProductFilterController(filterService, $scope) {
        var vm = this;
        vm.filterColorClass = "success"
        vm.filterType = "product"
        vm.availableTags = filterService.getAvailableProductFilters()
        vm.selectedTags = filterService.getSelectedProductFilters()

        $scope.$on('FILTERS_UPDATED', function(event) {
            vm.updateTags();
        });

        vm.removeTag = function(tag) {
            filterService.removeProductFilter(tag)
            vm.updateTags()
        }

        vm.addTag = function(tag) {
            filterService.addProductFilter(tag)
            vm.updateTags()
        }

        vm.removeAllTags = function() {
            filterService.removeAllProductFilters()
            vm.updateTags()
        }

        vm.addAllTags = function() {
            filterService.addAllProductFilters()
            vm.updateTags()
        }

        vm.updateTags = function() {
            vm.availableTags = filterService.getAvailableProductFilters()
            vm.selectedTags = filterService.getSelectedProductFilters()
        }
    }
})();
