(function() {
    "user strict"
    angular.module("gitReqs").controller('LevelFilterController', ['filterService', '$scope', LevelFilterController]);

    function LevelFilterController(filterService, $scope) {
        var vm = this;
        vm.filterType = "level"
        vm.filterColorClass = "warning"
        vm.availableTags = filterService.getAvailableLevelFilters()
        vm.selectedTags = filterService.getSelectedLevelFilters()

        $scope.$on('FILTERS_UPDATED', function(event) {
            vm.updateTags();
        });

        vm.removeTag = function(tag) {
            filterService.removeLevelFilter(tag)
            vm.updateTags()
        }

        vm.addTag = function(tag) {
            filterService.addLevelFilter(tag)
            vm.updateTags()
        }

        vm.removeAllTags = function() {
            filterService.removeAllLevelFilters()
            vm.updateTags()
        }

        vm.addAllTags = function() {
            filterService.addAllLevelFilters()
            vm.updateTags()
        }

        vm.updateTags = function() {
            vm.availableTags = filterService.getAvailableLevelFilters()
            vm.selectedTags = filterService.getSelectedLevelFilters()
        }
    }
})();
