(function() {
    "use strict"
    angular.module("gitReqs").controller('ReleaseFilterController', ['filterService', '$scope', ReleaseFilterController]);

    function ReleaseFilterController(filterService, $scope) {
        var vm = this;
        vm.filterColorClass = "info"
        vm.filterType = "release"
        vm.availableTags = filterService.getAvailableReleaseFilters()
        vm.selectedTags = filterService.getSelectedReleaseFilters()

        $scope.$on('FILTERS_UPDATED', function(event) {
            vm.updateTags();
        });

        vm.removeTag = function(tag) {
            filterService.removeReleaseFilter(tag)
            vm.updateTags()
        }

        vm.addTag = function(tag) {
            filterService.addReleaseFilter(tag)
            vm.updateTags()
        }

        vm.removeAllTags = function() {
            filterService.removeAllReleaseFilters()
            vm.updateTags()
        }

        vm.addAllTags = function() {
            filterService.addAllReleaseFilters()
            vm.updateTags()
        }

        vm.updateTags = function() {
            vm.availableTags = filterService.getAvailableReleaseFilters()
            vm.selectedTags = filterService.getSelectedReleaseFilters()
        }
    }
})();
