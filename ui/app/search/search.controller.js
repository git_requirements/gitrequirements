(function() {
  "user strict"
  angular.module("gitReqs").controller('SearchController', ['filterService', SearchController]);

  function SearchController(filterService) {
    var vm = this
    vm.query = ""

    vm.updateQuery = function(){
      console.log(vm.query)
      filterService.setQuery(vm.query)
    }
  }

})();
