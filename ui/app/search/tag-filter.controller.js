(function() {
  "use strict"
  angular.module("gitReqs").controller('TagFilterController', ['filterService', '$scope', TagFilterController]);

  function TagFilterController(filterService, $scope) {
    var vm = this;
    vm.filterColorClass = "danger"
    vm.filterType = "tag"
      //TODO logika dot. tagow
    vm.availableTags = filterService.getAvailableTagFilters()
    vm.selectedTags = filterService.getSelectedTagFilters()

    $scope.$on('FILTERS_UPDATED', function(event) {
      vm.updateTags();
    });

    vm.removeTag = function(tag) {
      filterService.removeTagFilter(tag)
      vm.updateTags()
    }

    vm.addTag = function(tag) {
      filterService.addTagFilter(tag)
      vm.updateTags()
    }

    vm.removeAllTags = function() {
      filterService.removeAllTagFilters()
      vm.updateTags()
    }

    vm.addAllTags = function() {
      filterService.addAllTagFilters()
      vm.updateTags()
    }

    vm.updateTags = function() {
      vm.availableTags = filterService.getAvailableTagFilters()
      vm.selectedTags = filterService.getSelectedTagFilters()
    }
  }
})();
