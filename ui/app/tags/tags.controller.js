(function() {
  "user strict"
  angular.module("gitReqs").controller('TagsController', ['filterService', 'tagsResource', 'toastr', '$uibModal', '$scope', '$log', TagsController]);

  function TagsController(filterService, tagsResource, toastr, $uibModal, $scope, $log) {
    var vm = this;

    vm.currentPage = 1
    vm.maxPages = 3
    vm.numPerPage = 5

    vm.reqsSelected = []

    vm.tagsNr = tagsResource.query({
      nr: "get"
    })

    vm.foundTags = tagsResource.query({
      begin: [vm.currentPage],
      limit: [vm.numPerPage]
    })

    vm.update = function() {
      vm.foundTags = tagsResource.query({
        begin: [vm.currentPage],
        limit: [vm.numPerPage]
      })

      vm.isAllSelected = false
    }


    vm.openTagPreview = function(tag) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/tags/tag-view-modal.html',
        controller: 'TagViewModalController',
        controllerAs: 'vm',
        size: '',
        resolve: {
          tag: function() {
            return tag;
          }
        }
      });
    };

    vm.openTagEdit = function(tag) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/tags/tag-edit-modal.html',
        controller: 'TagEditModalController',
        controllerAs: 'vm',
        size: '',
        resolve: {
          tag: function() {
            return tag;
          }
        }
      });

      modalInstance.result.then(function(item) {
        vm.tag = item;
        if (!vm.tag.id) {
          vm.tag.id = 0
        }
        tagsResource.save({
          'id': vm.tag.id,
          'name': vm.tag.name,
          'desciption': vm.tag.description
        }, function() {
          toastr.success("Successfully saved: " + vm.tag.name);
          vm.foundTags = tagsResource.query({
            begin: [vm.currentPage],
            limit: [vm.numPerPage]
          })
        });
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };



    vm.openTagRemove = function(tag) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/tags/tag-remove-modal.html',
        controller: 'TagRemoveModalController',
        controllerAs: 'vm',
        size: '',
        resolve: {
          tag: function() {
            return tag;
          }
        }
      });

      modalInstance.result.then(function(item) {
        vm.tag = item;
        tagsResource.delete({
          'id': vm.tag.id
        })

        vm.foundTags = tagsResource.query({
          begin: [vm.currentPage],
          limit: [vm.numPerPage]
        })
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };


    $scope.$on('REQUIREMENT_UPDATED', function(event) {
      vm.foundTags = tagsResource.query()
    });

  }
})();
