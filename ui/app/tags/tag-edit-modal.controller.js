(function() {
  "use strict"
  // Please note that $modalInstance represents a modal window (instance) dependency.
  // It is not the same as the $uibModal service used above.

  angular.module('gitReqs').controller('TagEditModalController', ['$uibModalInstance', 'tag',
    TagEditModalController
  ]);

  function TagEditModalController($uibModalInstance, tag) {
    var vm = this

    vm.tag = {
      id: 0
    }

    if (tag && tag.id > 0) {
      vm.tag = {
        id: tag.id,
        name: tag.name,
        description: tag.description
      }
      vm.title = "Edit tag..."
    } else {
      vm.title = "Create new tag..."
    }

    vm.close = function() {
      $uibModalInstance.dismiss('cancel');
    };

    vm.save = function() {
      $uibModalInstance.close(vm.tag);
    };
  };
})()
