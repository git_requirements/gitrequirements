(function() {
  "use strict"
  angular.module('gitReqs').controller('TagViewModalController', ['$uibModalInstance', 'tag', TagViewModalController]);

  function TagViewModalController($uibModalInstance, tag) {
    var vm = this

    vm.tag = tag

    vm.close = function() {
      $uibModalInstance.dismiss('cancel');
    };


  };
})()
