(function() {
  "use strict"
  // Please note that $modalInstance represents a modal window (instance) dependency.
  // It is not the same as the $uibModal service used above.

  angular.module('gitReqs').controller('TagRemoveModalController', ['$uibModalInstance', 'tag',
    TagRemoveModalController
  ]);

  function TagRemoveModalController($uibModalInstance, tag) {
    var vm = this

    vm.tag = tag
    vm.title = "Remove tag..."


    vm.close = function() {
      $uibModalInstance.dismiss('cancel');
    };

    vm.remove = function(tag) {
      $uibModalInstance.close(vm.tag);
    };
  };
})()
