(function() {
  "use strict"
  angular.module('gitReqs').service('filterService', ['$rootScope', 'tagsResource', filterService])

  function filterService($rootScope, tagsResource) {
    var svc = this;

    svc.query = ""

    svc.broadcastFiltersUpdated = function() {
      $rootScope.$broadcast('FILTERS_UPDATED');
    }

    svc.allSystemFilters = sysFilters;
    svc.selectedSystemFilters = [];
    svc.availableSystemFiltrs = svc.allSystemFilters.slice(0);

    svc.setQuery = function(query) {
      svc.query = query
    }

    svc.getQuery = function() {
      return svc.query
    }



    svc.getAvailableSystemFilters = function() {
      return svc.availableSystemFiltrs;
    }

    svc.getSelectedSystemFilters = function() {
      return svc.selectedSystemFilters;
    }

    svc.getSelectedSystemFilterIds = function() {
      return svc.getFilterIds(svc.getSelectedSystemFilters());
    }

    svc.addSystemFilter = function(filter) {
      svc.selectedSystemFilters.push(filter);
      svc.availableSystemFiltrs.splice(svc.availableSystemFiltrs.indexOf(filter), 1)
      svc.broadcastFiltersUpdated();
    }

    svc.removeSystemFilter = function(filter) {
      svc.availableSystemFiltrs.push(filter);
      svc.selectedSystemFilters.splice(svc.selectedSystemFilters.indexOf(filter), 1)
    }

    svc.addAllSystemFilters = function() {
      svc.selectedSystemFilters = svc.selectedSystemFilters.concat(svc.availableSystemFiltrs);
      svc.availableSystemFiltrs = [];
    }

    svc.removeAllSystemFilters = function() {
      svc.availableSystemFiltrs = svc.availableSystemFiltrs.concat(svc.selectedSystemFilters);
      svc.selectedSystemFilters = [];
    }

    //Product
    svc.allProductFilters = prodFilters;
    svc.selectedProductFilters = [];
    svc.availableProductFiltrs = svc.allProductFilters.slice(0);

    svc.getAvailableProductFilters = function() {
      return svc.availableProductFiltrs;
    }

    svc.getSelectedProductFilters = function() {
      return svc.selectedProductFilters;
    }

    svc.getAllProductFilters = function() {
      return svc.availableProductFiltrs.concat(svc.selectedProductFilters);
    }

    svc.getSelectedProductFilterIds = function() {
      return svc.getFilterIds(svc.getSelectedProductFilters());
    }

    svc.addProductFilter = function(filter) {
      svc.selectedProductFilters.push(filter);
      svc.availableProductFiltrs.splice(svc.availableProductFiltrs.indexOf(filter), 1)
    }

    svc.removeProductFilter = function(filter) {
      svc.availableProductFiltrs.push(filter);
      svc.selectedProductFilters.splice(svc.selectedProductFilters.indexOf(filter), 1)
    }

    svc.addAllProductFilters = function() {
      svc.selectedProductFilters = svc.selectedProductFilters.concat(svc.availableProductFiltrs);
      svc.availableProductFiltrs = [];
    }

    svc.removeAllProductFilters = function() {
      svc.availableProductFiltrs = svc.availableProductFiltrs.concat(svc.selectedProductFilters);
      svc.selectedProductFilters = [];
    }

    //Tags
    svc.allTagFilters = tagFilters
    svc.selectedTagFilters = [];
    svc.availableTagFiltrs = svc.allTagFilters.slice(0);

    svc.getAvailableTagFilters = function() {
      return svc.availableTagFiltrs;
    }

    svc.getSelectedTagFilters = function() {
      return svc.selectedTagFilters;
    }

    svc.getAllTags = function() {
      return svc.allTagFilters;
    }

    svc.getSelectedTagFilterIds = function() {
      return svc.getFilterIds(svc.getSelectedTagFilters());
    }

    svc.addTagFilter = function(filter) {
      svc.selectedTagFilters.push(filter);
      svc.availableTagFiltrs.splice(svc.availableTagFiltrs.indexOf(filter), 1)
    }

    svc.removeTagFilter = function(filter) {
      svc.availableTagFiltrs.push(filter);
      svc.selectedTagFilters.splice(svc.selectedTagFilters.indexOf(filter), 1)
    }

    svc.addAllTagFilters = function() {
      svc.selectedTagFilters = svc.selectedTagFilters.concat(svc.availableTagFiltrs);
      svc.availableTagFiltrs = [];
    }

    svc.removeAllTagFilters = function() {
      svc.availableTagFiltrs = svc.availableTagFiltrs.concat(svc.selectedTagFilters);
      svc.selectedTagFilters = [];
    }

    //Release
    svc.allReleaseFilters = relFilters;
    svc.selectedReleaseFilters = [];
    svc.availableReleaseFiltrs = relFilters.slice(0);

    svc.getAvailableReleaseFilters = function() {
      return svc.availableReleaseFiltrs;
    }

    svc.getSelectedReleaseFilters = function() {
      return svc.selectedReleaseFilters;
    }

    svc.getAllReleaseFilters = function() {
      return svc.availableReleaseFiltrs.concat(svc.selectedReleaseFilters);
    }


    svc.getSelectedReleaseFilterIds = function() {
      return svc.getFilterIds(svc.getSelectedReleaseFilters());
    }

    svc.addReleaseFilter = function(filter) {
      svc.selectedReleaseFilters.push(filter);
      svc.availableReleaseFiltrs.splice(svc.availableReleaseFiltrs.indexOf(filter), 1)
    }

    svc.removeReleaseFilter = function(filter) {
      svc.availableReleaseFiltrs.push(filter);
      svc.selectedReleaseFilters.splice(svc.selectedReleaseFilters.indexOf(filter), 1)
    }

    svc.addAllReleaseFilters = function() {
      svc.selectedReleaseFilters = svc.selectedReleaseFilters.concat(svc.availableReleaseFiltrs);
      svc.availableReleaseFiltrs = [];
    }

    svc.removeAllReleaseFilters = function() {
        svc.availableReleaseFiltrs = svc.availableReleaseFiltrs.concat(svc.selectedReleaseFilters);
        svc.selectedReleaseFilters = [];
      }
      //Level
    svc.allLevelFilters = levelFilters;
    svc.selectedLevelFilters = [];
    svc.availableLevelFiltrs = svc.allLevelFilters.slice(0);

    svc.getAvailableLevelFilters = function() {
      return svc.availableLevelFiltrs;
    }

    svc.getSelectedLevelFilters = function() {
      return svc.selectedLevelFilters;
    }

    svc.getAllLevelFilters = function() {
      return svc.availableLevelFiltrs.concat(svc.selectedLevelFilters);
    }


    svc.getSelectedLevelFilterIds = function() {
      return svc.getFilterIds(svc.getSelectedLevelFilters());
    }

    svc.addLevelFilter = function(filter) {
      svc.selectedLevelFilters.push(filter);
      svc.availableLevelFiltrs.splice(svc.availableLevelFiltrs.indexOf(filter), 1)
    }

    svc.removeLevelFilter = function(filter) {
      svc.availableLevelFiltrs.push(filter);
      svc.selectedLevelFilters.splice(svc.selectedLevelFilters.indexOf(filter), 1)
    }

    svc.addAllLevelFilters = function() {
      svc.selectedLevelFilters = svc.selectedLevelFilters.concat(svc.availableLevelFiltrs);
      svc.availableLevelFiltrs = [];
    }

    svc.removeAllLevelFilters = function() {
      svc.availableLevelFiltrs = svc.availableLevelFiltrs.concat(svc.selectedLevelFilters);
      svc.selectedLevelFilters = [];
    }

    svc.getFilterIds = function(filtersArray) {
      var filterIds = [];
      filtersArray.forEach(function(entry) {
        filterIds.push(entry.id);
      });
      return filterIds;
    }

  };
})();
