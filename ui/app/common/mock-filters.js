var sysFilters = [{
    "id": 1,
    "name": "LTE"
}, {
    "id": 2,
    "name": "Astro"
}, {
    "id": 3,
    "name": "Dimetra"
}]


var prodFilters = [{
    "id": 1,
    "name": "AuC"
}, {
    "id": 2,
    "name": "KMF"
}, {
    "id": 3,
    "name": "CMF"
}, {
    "id": 4,
    "name": "KVL"
}, {
    "id": 5,
    "name": "Common Web"
}, {
    "id": 6,
    "name": "Standy"
}, {
    "id": 7,
    "name": "License Manager"
}, {
    "id": 8,
    "name": "SPA"
}, {
    "id": 9,
    "name": "ZDS"
}, {
    "id": 10,
    "name": "UEM"
}, {
    "id": 11,
    "name": "UNC"
}, {
    "id": 12,
    "name": "Zone Watch"
}, {
    "id": 13,
    "name": "PTT"
}, {
    "id": 14,
    "name": "Conosle"
}]

var tagFilters = [{
    "id": 1,
    "name": "Login"
}, {
    "id": 2,
    "name": "Logout"
}, {
    "id": 3,
    "name": "View"
}, {
    "id": 4,
    "name": "Security"
}, {
    "id": 5,
    "name": "Session"
}, {
    "id": 6,
    "name": "Layout"
}, {
    "id": 7,
    "name": "Users"
}]

var sysProdMap = [{
    "sysId": 1,
    "prodId": [1, 2, 3, 4, 5, 6]
}, {
    "sysId": 2,
    "prodId": [7, 8, 9, 10, 11, 12, 13, 14]
}, {
    "sysId": 3,
    "prodId": [1, 3, 5, 7, 9, 11, 13]
}]

var relFilters = [{
    "id": 1,
    "name": "A7.16"
}, {
    "id": 2,
    "name": "A7.15"
}, {
    "id": 3,
    "name": "D8.3"
}, {
    "id": 4,
    "name": "D8.2"
}, {
    "id": 5,
    "name": "D8.1"
}, {
    "id": 6,
    "name": "A7.13"
}, {
    "id": 7,
    "name": "D8.0"
}]

var levelFilters = [{
    "id": 1,
    "name": "L1"
}, {
    "id": 2,
    "name": "L2"
}, {
    "id": 3,
    "name": "L3"
}, {
    "id": 4,
    "name": "L4"
}]


var mockRquirements = [{
    "id": 1,
    "name": "Login page",
    "systems": [1]
}, {
    "id": 2,
    "name": "Hello popup",
    "systems": [1, 2]
}, {
    "id": 3,
    "name": "Password requirements",
    "systems": [1, 2, 3]
}, {
    "id": 4,
    "name": "User management",
    "systems": [1]
}, {
    "id": 5,
    "name": "Session timeout",
    "systems": [1, 2]
}, {
    "id": 6,
    "name": "Logout page",
    "systems": [1, 2, 3]
}]
