(function() {
  "use strict"
  angular.module('gitReqs').service('levelService', [levelService])

  function levelService() {
    var svc = this;

    svc.L1 = {
      "id": 1,
      "name": "L1"
    }

    svc.L2 = {
      "id": 2,
      "name": "L2"
    }
    svc.L3 = {
      "id": 3,
      "name": "L3",
    }
    svc.L4 = {
      "id": 4,
      "name": "L4"
    }

    svc.getLoLevelFilterFor = function(level) {
      switch (level.id) {
        case 1:
          return svc.L2
        case 2:
          return svc.L3
        case 3:
          return svc.L4
        default:
          return {}
      }
    }

    svc.isLowestLevel = function(level){
      console.log("Return: " + (level.id == svc.L4.id))
      return level.id == svc.L4.id
    }
  };
})();
