(function() {
  "use strict"
  angular.module("gitReqs").directive('grDiffTagReq', ['$compile', grDiffTagReq]);

  function grDiffTagReq($compile) {
    return {
      restrict: "E",
      templateUrl: "app/requirement/diff-tag.html",
      scope: {},
      controller: "ReqDiffTagController",
      controllerAs: "vm",
      bindToController: {
        diff: '=diff',
        view: '=view',
        desc: '@'
      },
      link: function link(scope, element, attributes) {
        $compile(element.contents())(scope);
      },
    }
  }
})()
