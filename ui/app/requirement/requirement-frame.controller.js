(function() {
  "use strict"
  angular.module("gitReqs").controller('RequirementFrameController', [RequirementDetailsController]);

  function RequirementDetailsController() {
    var vm = this;
    vm.collapsed = (vm.initialCollapsed === 'true')

    vm.toggleCollapse = function() {
      vm.collapsed = !vm.collapsed
    }

    vm.getCollapseIndicator = function() {
      if (vm.collapsed) {
        return "glyphicon glyphicon-chevron-right pull-left"
      } else {
        return "glyphicon glyphicon-chevron-down pull-left"
      }
    }

    vm.getSwitchViewBtn = function() {
      if (vm.view === "diff") {
        return "Content view"
      } else {
        return "Diff view"
      }
    }

    vm.toggleView = function() {
      if (vm.view === 'diff') {
        vm.view = 'content'
      } else {
        vm.view = 'diff'
      }
    }

  }

})();
