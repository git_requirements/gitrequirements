(function() {
  "use strict"
  angular.module("gitReqs").directive('grReqFrame', [grReqFrame]);

  function grReqFrame() {
    return {
      templateUrl: "app/requirement/requirement-frame.html",
      restrict: "E",
      scope: {
        req: '='
      },
      controller: "RequirementFrameController",
      bindToController: {
        initialCollapsed: '@collapsed',
        view: '='
      },
      controllerAs: "vm",
      transclude: true
    }
  }
})()
