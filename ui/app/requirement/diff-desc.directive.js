(function() {
  "use strict"
  angular.module("gitReqs").directive('grDiffDescReq', ['$compile', grDiffDescReq]);

  function grDiffDescReq($compile) {
    return {
      restrict: "E",
      templateUrl: "app/requirement/diff-desc.html",
      scope: {},
      controller: "ReqDiffDescController",
      controllerAs: "vm",
      bindToController: {
        diff: '=diff',
        view: '=view'
      },
      link: function link(scope, element, attributes) {
        //element.html(scope.vm.getTemplateUrl())
        $compile(element.contents())(scope);

      },
    }
  }
})()
