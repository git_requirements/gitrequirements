(function() {
  "user strict"
  angular.module("gitReqs").controller('ReqDiffDescController', [ReqDiffDescController]);

  function ReqDiffDescController() {
    var vm = this;

    vm.showAdd = function() {
      vm.diffView = (vm.view === 'diff');
      vm.isAddView = (vm.diff.type === 'add');
      return vm.diffView && vm.isAddView;
    }

    vm.showRem = function() {
      vm.diffView = (vm.view === 'diff');
      vm.isRemView = (vm.diff.type === 'rem');
      return vm.diffView && vm.isRemView;
    }

    vm.showOrg = function() {
      vm.diffView = (vm.view === 'diff');
      vm.isAddView = (vm.diff.type === 'add');
      vm.isOrgView = (vm.diff.type === 'org');


      if (vm.diffView) {
        return vm.isOrgView;
      } else {
        return vm.isOrgView || vm.isAddView;
      }
    }
  }
})()
