(function() {
  "use strict"
  angular.module("gitReqs").directive('grWrkspcReq', [grWrkspcReq]);

  function grWrkspcReq() {
    return {
      templateUrl: "app/requirement/wrkspc-requirement.html",
      restrict: "E",
      scope: {
        requirement: '='
      },
      controller: "WrkspcRequirementFrameController",
      controllerAs: "vm",
      compile: function compile(element, attributes) {
        element.css("border", "1px solid #cccccc");
        return function link(scope, element, attributes) {}
      },
      replace: true
    }
  }
})()
