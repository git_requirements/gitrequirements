(function() {
  "use strict"
  angular.module("gitReqs").directive('grReq', [grReq]);

  function grReq() {
    return {
      templateUrl: "app/requirement/requirement.html",
      restrict: "E",
      scope: {
        req: '='
      },
      bindToController: {
        view: '='
      },
      controller: "RequirementController",
      controllerAs: "vm",
      compile: function compile(element, attributes) {
        element.css("border", "1px solid #cccccc");
        return function link(scope, element, attributes) {}
      },
      replace: true
    }
  }
})()
