(function() {
  "use strict"
  angular.module("gitReqs").controller('WrkspcRequirementFrameController', [WrkspcRequirementFrameController]);

  function WrkspcRequirementFrameController() {
    var vm = this;
    vm.viewType = "diff"
  }
})()
