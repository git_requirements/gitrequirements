(function() {
  var app = angular.module('gitReqs', ["common.services", "ui.router", "toastr", "ncy-angular-breadcrumb", "ui.bootstrap", "requirementsResourceMock"]);

  app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
    $stateProvider.state("workspace", {
        url: "/",
        templateUrl: "app/workspace/workspace.html",
        ncyBreadcrumb: {
          label: 'Your Workspace'
        }
      }).state("search", {
        abstract: true,
        templateUrl: "app/search/search.html",
        controller: "SearchController as vm"
      }).state("search.filter", {
        url: "/search",
        views: {
          "releaseFilter": {
            templateUrl: "app/search/filter.html",
            controller: "ReleaseFilterController as vm"
          },
          "productFilter": {
            templateUrl: "app/search/filter.html",
            controller: "ProductFilterController as vm"
          },
          "levelFilter": {
            templateUrl: "app/search/filter.html",
            controller: "LevelFilterController as vm"
          },
          "tagFilter": {
            templateUrl: "app/search/filter.html",
            controller: "TagFilterController as vm"
          }
        },
        ncyBreadcrumb: {
          label: 'Search Requirements'
        }
      }).state("requirements", {
        url: "/requirements",
        templateUrl: 'app/requirements/requirements.html',
        controller: 'RequirementsController as vm',
        resolve: {
          requirementsResource: "requirementsResource",
          infoResource: "infoResource",
          filterService: "filterService"
        },
        ncyBreadcrumb: {
          label: 'Found Requirements'
        }
      }).state("requirements.details", {
        url: '/:id',
        templateUrl: 'app/requirements/requirement-details.html',
        controller: "RequirementDetailsController as vm",
        resolve: {
          requirementsResource: "requirementsResource",
          requirement: function($stateParams, requirementsResource) {
            var id = $stateParams.id;
            return requirementsResource.get({
              'id': id
            }).$promise;
          }
        },
        ncyBreadcrumb: {
          label: 'Requirement'
        }
      })
      .state("requirements.details.edit", {
        url: '/edit/:parentId',
        templateUrl: 'app/requirements/requirement-edit.html',
        controller: "RequirementEditController as vm",
        resolve: {
          parentRequirement: function($stateParams, requirementsResource) {
            var parentId = $stateParams.parentId;
            if (parentId) {
              return requirementsResource.get({
                'id': parentId
              }).$promise;
            }
          }
        },
        ncyBreadcrumb: {
          label: 'Edit'
        }
      }).state("releases", {
        url: "/releases",
        templateUrl: 'app/releases/releases.html',
        controller: 'ReleasesController as vm',
        resolve: {
          filterService: "filterService",
          releasesResource: "releasesResource"
        },
        ncyBreadcrumb: {
          label: 'Releases'
        }
      }).state("products", {
        url: "/products",
        templateUrl: 'app/products/products.html',
        controller: 'ProductsController as vm',
        resolve: {
          requirementsResource: "releasesResource",
          filterService: "filterService"
        },
        ncyBreadcrumb: {
          label: 'Products'
        }
      }).state("tags", {
        url: "/tags",
        templateUrl: 'app/tags/tags.html',
        controller: 'TagsController as vm',
        resolve: {
          requirementsResource: "releasesResource",
          filterService: "filterService"
        },
        ncyBreadcrumb: {
          label: 'Tags'
        }
      })

    $urlRouterProvider.otherwise('/');
  }]);

  app.config(function($provide) {
    $provide.decorator('$exceptionHandler', ['$delegate',
      function($delegate) {
        return function(exception, cause) {
          exception.message = "Unexpected error." + exception.message;
          $delegate(exception, cause);
          //alert(exception.message);
        }
      }
    ])
  });
}());
