(function() {
  "use strict"
  // Please note that $modalInstance represents a modal window (instance) dependency.
  // It is not the same as the $uibModal service used above.

  angular.module('gitReqs').controller('ReleaseEditModalController', ['$uibModalInstance', 'release',
    ReleaseEditModalController
  ]);

  function ReleaseEditModalController($uibModalInstance, release) {
    var vm = this

    vm.release = {
      id: 0
    }

    if (release && release.id > 0) {
      vm.release = {
        id: release.id,
        name: release.name,
        description: release.description
      }
      vm.title = "Edit release..."
    } else {
      vm.title = "Create new release..."
    }

    vm.close = function() {
      $uibModalInstance.dismiss('cancel');
    };

    vm.save = function() {
      $uibModalInstance.close(vm.release);
    };
  };
})()
