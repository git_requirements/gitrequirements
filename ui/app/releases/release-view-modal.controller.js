(function() {
  "use strict"
  angular.module('gitReqs').controller('ReleaseViewModalController', ['$uibModalInstance', 'release', ReleaseViewModalController]);

  function ReleaseViewModalController($uibModalInstance, release) {
    var vm = this

    vm.release = release

    vm.close = function() {
      $uibModalInstance.dismiss('cancel');
    };


  };
})()
