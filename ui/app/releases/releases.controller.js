(function() {
  "user strict"
  angular.module("gitReqs").controller('ReleasesController', ['filterService', 'releasesResource', 'toastr', '$uibModal', '$scope', '$log', ReleasesController]);

  function ReleasesController(filterService, releasesResource, toastr, $uibModal, $scope, $log) {
    var vm = this;

    vm.currentPage = 1
    vm.maxPages = 3
    vm.numPerPage = 5

    vm.releasesNr = releasesResource.query({
      nr: "get"
    })

    vm.foundReleases = releasesResource.query({
      begin: [vm.currentPage],
      limit: [vm.numPerPage]
    })

    vm.update = function() {
      vm.foundReleases = releasesResource.query({
        begin: [vm.currentPage],
        limit: [vm.numPerPage]
      })

      vm.isAllSelected = false
    }

    vm.openReleasePreview = function(release) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/releases/release-view-modal.html',
        controller: 'ReleaseViewModalController',
        controllerAs: 'vm',
        size: '',
        resolve: {
          release: function() {
            return release;
          }
        }
      });
    };

    vm.openReleaseEdit = function(release) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/releases/release-edit-modal.html',
        controller: 'ReleaseEditModalController',
        controllerAs: 'vm',
        size: '',
        resolve: {
          release: function() {
            return release;
          }
        }
      });

      modalInstance.result.then(function(item) {
        vm.release = item;
        releasesResource.save({
          'id': vm.release.id,
          'name': vm.release.name,
          'desciption': vm.release.description
        }, function() {
          toastr.success("Successfully saved: " + vm.release.name);
          vm.foundReleases = releasesResource.query({
            begin: [vm.currentPage],
            limit: [vm.numPerPage]
          })
        });
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.$on('REQUIREMENT_UPDATED', function(event) {
      vm.foundReleases = releasesResource.query()
    });

  }
})();
