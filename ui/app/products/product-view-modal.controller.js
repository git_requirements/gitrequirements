(function() {
  "use strict"
  angular.module('gitReqs').controller('ProductViewModalController', ['$uibModalInstance', 'product', ProductViewModalController]);

  function ProductViewModalController($uibModalInstance, product) {
    var vm = this

    vm.product = product
    console.log(product)
    vm.close = function() {
      $uibModalInstance.dismiss('cancel');
    };


  };
})()
