(function() {
  "use strict"
  // Please note that $modalInstance represents a modal window (instance) dependency.
  // It is not the same as the $uibModal service used above.

  angular.module('gitReqs').controller('ProductRemoveModalController', ['$uibModalInstance', 'product',
    ProductRemoveModalController
  ]);

  function ProductRemoveModalController($uibModalInstance, product) {
    var vm = this

    vm.product = product
    vm.title = "Remove product..."


    vm.close = function() {
      $uibModalInstance.dismiss('cancel');
    };

    vm.remove = function(product) {
      $uibModalInstance.close(vm.product);
    };
  };
})()
