(function() {
  "use strict"
  // Please note that $modalInstance represents a modal window (instance) dependency.
  // It is not the same as the $uibModal service used above.

  angular.module('gitReqs').controller('ProductEditModalController', ['$uibModalInstance', 'product',
    ProductEditModalController
  ]);

  function ProductEditModalController($uibModalInstance, product) {
    var vm = this

    vm.product = {
      id: 0
    }

    if (product && product.id > 0) {
      vm.product = {
        id: product.id,
        name: product.name,
        description: product.description
      }

      vm.title = "Edit product..."
    } else {
      vm.title = "Create new product..."
    }

    vm.close = function() {
      $uibModalInstance.dismiss('cancel');
    };

    vm.save = function() {
      $uibModalInstance.close(vm.product);
    };
  };
})()
