(function() {
  "user strict"
  angular.module("gitReqs").controller('ProductsController', ['filterService', 'productsResource', 'toastr', '$uibModal', '$scope', '$log', ProductsController]);

  function ProductsController(filterService, productsResource, toastr, $uibModal, $scope, $log) {
    var vm = this;

    vm.currentPage = 1
    vm.maxPages = 3
    vm.numPerPage = 5

    vm.productsNr = productsResource.query({
      nr: "get"
    })

    vm.foundProducts = productsResource.query({
      begin: [vm.currentPage],
      limit: [vm.numPerPage]
    })

    vm.update = function() {
      vm.foundProducts = productsResource.query({
        begin: [vm.currentPage],
        limit: [vm.numPerPage]
      })

    }

    vm.openProductPreview = function(product) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/products/product-view-modal.html',
        controller: 'ProductViewModalController',
        controllerAs: 'vm',
        size: '',
        resolve: {
          product: function() {
            return product;
          }
        }
      });
    };

    vm.openProductRemove = function(product) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/products/product-remove-modal.html',
        controller: 'ProductRemoveModalController',
        controllerAs: 'vm',
        size: '',
        resolve: {
          product: function() {
            return product;
          }
        }
      });

      modalInstance.result.then(function(item) {
        vm.product = item;
        productsResource.delete({
          'id': vm.product.id
        })

        vm.foundProducts = productsResource.query({
          begin: [vm.currentPage],
          limit: [vm.numPerPage]
        })
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    vm.openProductEdit = function(product) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/products/product-edit-modal.html',
        controller: 'ProductEditModalController',
        controllerAs: 'vm',
        size: '',
        resolve: {
          product: function() {
            return product;
          }
        }
      });

      modalInstance.result.then(function(item) {
        vm.product = item;
        productsResource.save({
          'id': vm.product.id,
          'name': vm.product.name,
          'desciption': vm.product.description
        }, function() {
          toastr.success("Successfully saved: " + vm.product.name);
          vm.foundProducts = productsResource.query({
            begin: [vm.currentPage],
            limit: [vm.numPerPage]
          })
        });
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.$on('REQUIREMENT_UPDATED', function(event) {
      vm.foundProducts = productResource.query()
    });

  }
})();
