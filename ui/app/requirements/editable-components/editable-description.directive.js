(function() {
  "use strict"
  angular.module("gitReqs").directive('grEditableDescription', [grEditableDescription]);

  function grEditableDescription() {
    return {
      restrict: "E",
      templateUrl: "app/requirements/editable-components/editable-description.html",
      scope: {
        description: '=description',
        title: '@title',
        label: '@label',
        rows: '@rows'
      }
    }
  }
})()
