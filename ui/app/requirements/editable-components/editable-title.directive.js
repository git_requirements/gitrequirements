(function() {
  "use strict"
  angular.module("gitReqs").directive('grEditableTitle', [grEditableTitle]);

  function grEditableTitle() {
    return {
      restrict: "E",
      templateUrl: "app/requirements/editable-components/editable-title.html",
      scope: {
        description: '=description',
        title: '@title',
        label: '@label'
      }
    }
  }
})()
