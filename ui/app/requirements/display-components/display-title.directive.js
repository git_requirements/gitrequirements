(function() {
  "use strict"
  angular.module("gitReqs").directive('grDisplayTitle', [grDisplayTitle]);

  function grDisplayTitle() {
    return {
      restrict: "E",
      templateUrl: "app/requirements/display-components/display-title.html",
      scope: {
        description: '@description',
        title: '@title',
        label: '@label'
      }
    }
  }
})()
