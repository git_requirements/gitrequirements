(function() {
  "use strict"
  angular.module("gitReqs").directive('grDisplayTagsClickable', ['$compile', grDisplayTagsClickable]);

  function grDisplayTagsClickable($compile) {
    return {
      restrict: "E",
      templateUrl: "app/requirements/display-components/display-tags-clickable.html",
      //template: '<a class="button" ng-click="modalFunc()">Call home!</a>',
      scope: {},
      controller: "DisplayTagsClickableController",
      controllerAs: "vm",
      bindToController: {
        tags: '=tags',
        title: '@title',
        label: '@label',
        callback: '&'
      }
    }
  }
})()
