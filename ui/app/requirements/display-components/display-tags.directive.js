(function() {
  "use strict"
  angular.module("gitReqs").directive('grDisplayTags', [grDisplayTags]);

  function grDisplayTags() {
    return {
      restrict: "E",
      templateUrl: "app/requirements/display-components/display-tags.html",
      scope: {
        tags: '=tags',
        title: '@title',
        label: '@label'      }
    }
  }
})()
