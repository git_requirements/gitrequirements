(function() {
  "user strict"
  angular.module("gitReqs").controller('DisplayTagsClickableController', [DisplayTagsClickableController]);

  function DisplayTagsClickableController() {
    var vm = this;
    vm.notifyClick = function(tag) {
      vm.clickedTag = tag
      vm.callback({
        req: tag
      })
    }
  }
})()
