(function() {
  "use strict"
  angular.module("gitReqs").directive('grDisplayDescription', [grDisplayDescription]);

  function grDisplayDescription() {
    return {
      restrict: "E",
      templateUrl: "app/requirements/display-components/display-description.html",
      scope: {
        description: '@description',
        title: '@title',
        label: '@label',
        rows: '@rows'
      }
    }
  }
})()
