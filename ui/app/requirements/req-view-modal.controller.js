(function() {
  "use strict"
  // Please note that $modalInstance represents a modal window (instance) dependency.
  // It is not the same as the $uibModal service used above.

  angular.module('gitReqs').controller('ReqViewModalController', ['$uibModalInstance', 'requirementsResource', 'requirement',
    ReqViewModalController
  ]);

  function ReqViewModalController($uibModalInstance, requirementsResource, requirement) {
    var vm = this

    vm.requirement = requirementsResource.get({
      'id': requirement.id
    })

    vm.close = function() {
      $uibModalInstance.dismiss('cancel');
    };
  };
})()
