(function() {
  "user strict"
  angular.module("gitReqs").controller('RequirementsController', ['filterService', 'requirementsResource', 'infoResource',
    'toastr', '$uibModal', '$scope', '$log', RequirementsController
  ]);

  function RequirementsController(filterService, requirementsResource, infoResource, toastr, $uibModal, $scope, $log) {
    var vm = this;

    vm.currentPage = 1
    vm.maxPages = 3
    vm.numPerPage = 5

    vm.reqsSelected = []


    vm.reqsNr = infoResource.query({
      prd: [filterService.getSelectedProductFilterIds()],
      rel: [filterService.getSelectedReleaseFilterIds()],
      lvl: [filterService.getSelectedLevelFilterIds()],
    })

    vm.foundReqs = requirementsResource.query({
      prd: [filterService.getSelectedProductFilterIds()],
      rel: [filterService.getSelectedReleaseFilterIds()],
      lvl: [filterService.getSelectedLevelFilterIds()],
      begin: [vm.currentPage],
      limit: [vm.numPerPage],
      query: [filterService.getQuery()]
    })
    vm.isAllSelected = false

    vm.toggleSelection = function() {
      for (i = 0; i < vm.foundReqs.length; i++) {
        vm.foundReqs[i].isSelected = vm.isAllSelected;
        if (vm.isAllSelected) {
          selectReq(vm.foundReqs[i].id)
        } else {
          deselectReq(vm.foundReqs[i].id)
        }
      }
    }

    vm.toggleReqSelection = function(reqId) {
      var index = vm.reqsSelected.indexOf(reqId)
      if (index > -1) {
        vm.reqsSelected.splice(index, 1)
      } else {
        vm.reqsSelected.push(reqId)
      }
      console.log(vm.reqsSelected)
    }

    function deselectReq(reqId) {
      var index = vm.reqsSelected.indexOf(reqId)
      if (index > -1) {
        console.log("Deselect req: " + reqId)

        vm.reqsSelected.splice(index, 1)
      }
    }

    function selectReq(reqId) {
      var index = vm.reqsSelected.indexOf(reqId)
      if (index < 0) {
        console.log("Seelct req: " + reqId)

        vm.reqsSelected.push(reqId)
      }
    }

    vm.noneReqSelected = function() {
      return vm.reqsSelected.length == 0;
    }

    vm.systemFilters = filterService.getSelectedSystemFilters()
    vm.releaseFilters = filterService.getSelectedReleaseFilters()
    vm.productFilters = filterService.getSelectedProductFilters()
    vm.levelFilters = filterService.getSelectedLevelFilters()
    vm.tagFilters = filterService.getSelectedTagFilters()

    vm.update = function() {
      vm.foundReqs = requirementsResource.query({
        prd: [filterService.getSelectedProductFilterIds()],
        rel: [filterService.getSelectedReleaseFilterIds()],
        lvl: [filterService.getSelectedLevelFilterIds()],
        begin: [vm.currentPage],
        limit: [vm.numPerPage]
      })

      updatedReqsSelection()
      vm.isAllSelected = false
    }

    function updatedReqsSelection() {
      console.log("Update selection for:: " + vm.reqsSelected)
      vm.foundReqs.$promise.then(function(data) {
        for (req of vm.foundReqs) {
          if (vm.reqsSelected.indexOf(req.id) > -1) {
            console.log("Select req with id:" + req.id)
            req.isSelected = true
          } else {
            req.isSelected = false
          }
        }
      })
    }

    vm.openReleaseChooser = function(size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/release-add-modal.html',
        controller: 'ReleaseAddModalController',
        controllerAs: 'mod',
        size: size,
        resolve: {
          items: function() {
            return vm.releaseFilters
          }
        }
      });

      modalInstance.result.then(function(selectedItem) {
        vm.selected = selectedItem;
        if (vm.selected) {
          if (vm.selected) {
            for (req of vm.foundReqs) {
              for (reqId of vm.reqsSelected) {
                requirementsResource.get({
                  'id': reqId
                }).$promise.then(function(data) {
                  if (!data.releases) {
                    data.releases = []
                  }
                  data.releases = data.releases.concat(vm.selected)
                  data.$save(function(result) {});
                })
              }
            }
            toastr.success("Successfully applied releases");
          }
        }
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    vm.openProductChooser = function(size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/product-add-modal.html',
        controller: 'ProductAddModalController',
        controllerAs: 'mod',
        size: size,
        resolve: {
          items: function() {
            return vm.productFilters
          }
        }
      });

      modalInstance.result.then(function(selectedItem) {
        vm.selected = selectedItem;
        if (vm.selected) {
          if (vm.selected) {
            for (reqId of vm.reqsSelected) {
              requirementsResource.get({
                'id': reqId
              }).$promise.then(function(data) {
                if (!data.products) {
                  data.products = []
                }
                data.products = data.products.concat(vm.selected)
                data.$save(function(result) {});
              })
            }

            toastr.success("Successfully applied products");
          }
        }
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    vm.openTagsChooser = function(size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/tag-add-modal.html',
        controller: 'TagAddModalController',
        controllerAs: 'mod',
        size: size,
        resolve: {
          items: function() {
            return vm.tagFilters
          }
        }
      });

      modalInstance.result.then(function(selectedItem) {
        vm.selected = selectedItem;

        if (vm.selected) {
          for (req of vm.foundReqs) {
            for (reqId of vm.reqsSelected) {
              requirementsResource.get({
                'id': reqId
              }).$promise.then(function(data) {
                if (!data.products) {
                  data.tags = []
                }
                data.tags = data.tags.concat(vm.selected)
                data.$save(function(result) {});
              })
            }
          }
          toastr.success("Successfully applied tags");
        }

      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };


    vm.removeReq = function(reqId, size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/remove-reqs-modal.html',
        controller: 'RemoveReqsModalController',
        controllerAs: 'mod',
        size: size
      });

      modalInstance.result.then(function() {

        requirementsResource.delete({
          'id': reqId
        }).$promise.then(function(data) {
          vm.currentPage = 1
          vm.reqsNr = infoResource.query({
            prd: [filterService.getSelectedProductFilterIds()],
            rel: [filterService.getSelectedReleaseFilterIds()],
            lvl: [filterService.getSelectedLevelFilterIds()],
          })

          vm.foundReqs = requirementsResource.query({
            prd: [filterService.getSelectedProductFilterIds()],
            rel: [filterService.getSelectedReleaseFilterIds()],
            lvl: [filterService.getSelectedLevelFilterIds()],
            begin: [vm.currentPage],
            limit: [vm.numPerPage]
          })
        })


        toastr.success("Successfully removed requirements");

      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    vm.removeSelected = function(size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/remove-reqs-modal.html',
        controller: 'RemoveReqsModalController',
        controllerAs: 'mod',
        size: size
      });

      modalInstance.result.then(function() {
        for (reqId of vm.reqsSelected) {
          requirementsResource.delete({
            'id': reqId
          }).$promise.then(function(data) {
            vm.currentPage = 1
            vm.reqsNr = infoResource.query({
              prd: [filterService.getSelectedProductFilterIds()],
              rel: [filterService.getSelectedReleaseFilterIds()],
              lvl: [filterService.getSelectedLevelFilterIds()],
            })

            vm.isAllSelected = false
            vm.reqsSelected = []
            vm.foundReqs = requirementsResource.query({
              prd: [filterService.getSelectedProductFilterIds()],
              rel: [filterService.getSelectedReleaseFilterIds()],
              lvl: [filterService.getSelectedLevelFilterIds()],
              begin: [vm.currentPage],
              limit: [vm.numPerPage]
            })
          })
        }

        toastr.success("Successfully removed requirements");

      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };




    $scope.$on('REQUIREMENT_UPDATED', function(event) {
      vm.foundReqs = requirementsResource.query({
        prd: [filterService.getSelectedProductFilterIds()],
        rel: [filterService.getSelectedReleaseFilterIds()],
        lvl: [filterService.getSelectedLevelFilterIds()],
        begin: [vm.currentPage],
        limit: [vm.numPerPage],
        query: [filterService.getQuery()]
      })
    });

  }
})();
