(function() {
  "use strict"

  angular.module('gitReqs').controller('LoLevelSelectModalController', ['filterService', 'levelService', 'requirementsResource', 'infoResource', '$uibModalInstance', 'requirement',
    LoLevelSelectModalController
  ]);

  function LoLevelSelectModalController(filterService, levelService, requirementsResource, infoResource, $uibModalInstance, requirement) {
    var vm = this

    vm.currentPage = 1
    vm.maxPages = 3
    vm.numPerPage = 5

    vm.reqsNr = infoResource.query({
      prd: [filterService.getSelectedProductFilterIds()],
      rel: [filterService.getSelectedReleaseFilterIds()],
      lvl: [filterService.getSelectedLevelFilterIds()],
    })



    vm.update = function() {
      vm.requirements = requirementsResource.query({
        prd: [filterService.getSelectedProductFilterIds()],
        rel: [filterService.getSelectedReleaseFilterIds()],
        lvl: [filterService.getSelectedLevelFilterIds()],
        begin: [vm.currentPage],
        limit: [vm.numPerPage]
      })
    }


    vm.requirement = requirement;
    vm.loLevel = levelService.getLoLevelFilterFor(vm.requirement.level);

    vm.productIds = [];
    for (var i = 0; i < vm.requirement.products.length; i++) {
      vm.productIds.push(vm.requirement.products[i].id)
    }

    vm.releaseIds = [];
    for (var i = 0; i < vm.requirement.releases.length; i++) {
      vm.releaseIds.push(vm.requirement.releases[i].id)
    }

    vm.levelIds = [];
    vm.levelIds.push(vm.loLevel.id)

    vm.requirements = requirementsResource.query({
      "prd": [vm.productIds],
      "rel": [vm.releaseIds],
      "lvl": [vm.levelIds],
      "begin": [vm.currentPage],
      "limit": [vm.numPerPage]
    })

    //vm.requirements = array_diff(vm.requirements, vm.requirement.loLevel)


    vm.select = function(requirement) {
      $uibModalInstance.close(requirement);
    };

    vm.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };
  };

  function array_diff(arr1) {
    var retArr = {}
    argl = arguments.length,
      k1 = '',
      i = 1,
      k = '',
      arr = {};

    arr1keys: for (k1 in arr1) {
      for (i = 1; i < argl; i++) {
        arr = arguments[i];
        for (k in arr) {
          if (arr[k].id === arr1[k1].id) {
            // If it reaches here, it was found in at least one array, so try next value
            continue arr1keys;
          }
        }

        retArr[k1] = arr1[k1];
      }
    }

    return retArr;
  }

})()
