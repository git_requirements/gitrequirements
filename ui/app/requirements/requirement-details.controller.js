(function() {
  "use strict"
  angular.module("gitReqs").controller('RequirementDetailsController', ['filterService', 'requirement', '$uibModal', RequirementDetailsController]);

  function RequirementDetailsController(filterService, requirement, $uibModal) {
    var vm = this;
    vm.requirement = requirement;


    vm.openReqPreview = function(req) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/req-view-modal.html',
        controller: 'ReqViewModalController',
        controllerAs: 'vm',
        size: 'lg',
        resolve: {
          requirement: function() {
            return req;
          }
        }
      });
    };
  }

})();
