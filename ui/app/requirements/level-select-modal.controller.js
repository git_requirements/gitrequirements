(function() {
  // Please note that $modalInstance represents a modal window (instance) dependency.
  // It is not the same as the $uibModal service used above.

  angular.module('gitReqs').controller('LevelSelectModalController', ['filterService', '$uibModalInstance', 'item', LevelSelectModalController]);

  function LevelSelectModalController(filterService, $uibModalInstance, item) {
    var mod = this
    mod.notAppliedLevelFilters = filterService.getAllLevelFilters()
      //mod.notAppliedLevelFilters = array_diff(mod.notAppliedLevelFilters, [items])

    mod.items = mod.notAppliedLevelFilters;
    mod.selected = item

    mod.ok = function() {
      $uibModalInstance.close(mod.selected);
    };

    mod.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };

    mod.addSelected = function(item) {
      mod.selected = item
    }

  };

  function array_diff(arr1) {
    var retArr = {}
    argl = arguments.length,
      k1 = '',
      i = 1,
      k = '',
      arr = {};

    arr1keys: for (k1 in arr1) {
      for (i = 1; i < argl; i++) {
        arr = arguments[i];
        for (k in arr) {
          if (arr[k].id === arr1[k1].id) {
            // If it reaches here, it was found in at least one array, so try next value
            continue arr1keys;
          }
        }

        retArr[k1] = arr1[k1];
      }
    }

    return retArr;
  }

})()
