(function() {
  "user strict"
  angular.module("gitReqs").controller('RequirementEditController', ['filterService', 'levelService', 'requirementsResource', 'requirement', 'parentRequirement', 'toastr', '$state', '$uibModal', '$rootScope', '$log', RequirementEditController]);

  function RequirementEditController(filterService, levelService, requirementsResource, requirement, parentRequirement, toastr, $state, $uibModal, $rootScope, $log) {
    var vm = this;
    vm.requirement = requirement;
    vm.parentRequirement = parentRequirement

    if (vm.parentRequirement) {
      vm.requirement.hiLevel = {
        "id": vm.parentRequirement.id,
        "name": vm.parentRequirement.name,
      }

      var loLevel = levelService.getLoLevelFilterFor(vm.parentRequirement.level)
      if (loLevel) {
        vm.requirement.level = loLevel
      }

      vm.requirement.releases = vm.parentRequirement.releases
      vm.requirement.products = vm.parentRequirement.products

    } else {
      if (requirement.hiLevel && requirement.hiLevel.id > 0) {
        vm.parentRequirement = requirementsResource.get({
          'id': requirement.hiLevel.id

        })
      }
    }

    if (requirement && requirement.id > 0) {
      vm.title = "Edit requirement..."
    } else {
      vm.title = "Create new requirement..."
    }

    vm.submit = function(isFormValid) {
      if (isFormValid && vm.allRequiredTagSetup() && vm.requirement.name && vm.requirement.name != '' && vm.requirement.description && vm.requirement.description != '') {
        vm.requirement.$save(function(data) {
          toastr.success("Successfully saved: " + vm.requirement.name);
          vm.broadcastRequirementUpdated();
          $state.go("requirements.details", {
            id: data.id
          });

          //TODO clean this piece of crap
          temp = {

            "id": data.id,
            "name": vm.requirement.name,
          }
          if (vm.parentRequirement) {
            if (vm.parentRequirement.loLevel) {
              vm.parentRequirement.loLevel.push(temp)
            } else {
              vm.parentRequirement.loLevel = [];
              vm.parentRequirement.push(temp)
            }
            vm.parentRequirement.$save(function(data) {
              toastr.success("Successfully saved: " + vm.parentRequirement.name);
              vm.broadcastRequirementUpdated();
              //$state.go("requirements.details");

            });
          }
        });
      } else {
        toastr.error("Please fill the form correctly.")
      }
    }

    vm.cancel = function() {
      if (requirement && requirement.id > 0) {
        $state.go("requirements.details");
      } else {
        $state.go("workspace");
      }
    }

    vm.back = function() {
      if (requirement && requirement.id > 0) {
        $state.go("requirements.details");
      } else {
        $state.go("workspace");
      }
    }

    vm.broadcastRequirementUpdated = function() {
      $rootScope.$broadcast('REQUIREMENT_UPDATED');
    }

    //Release
    vm.removeRelease = function(tag) {
      vm.requirement.releases.splice(vm.requirement.releases.indexOf(tag), 1)
    }

    vm.noneReleasesThatApplies = function() {
      return !vm.requirement.releases || vm.requirement.releases.length == 0
    }


    vm.allRequiredTagSetup = function() {
      var lev = vm.requirement.level
      var rel = vm.requirement.releases && vm.requirement.releases.length > 0
      var prod = vm.requirement.products && vm.requirement.products.length > 0
      return lev && rel && prod
    }

    vm.openReleaseChooser = function(size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/release-add-modal.html',
        controller: 'ReleaseAddModalController',
        controllerAs: 'mod',
        size: size,
        resolve: {
          items: function() {
            return vm.requirement.releases;
          }
        }
      });

      modalInstance.result.then(function(selectedItem) {
        vm.selected = selectedItem;
        if (vm.selected) {
          if (!vm.requirement.releases) {
            vm.requirement.releases = []
          }
          vm.requirement.releases = vm.requirement.releases.concat(vm.selected)
        }
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    //System
    vm.removeProduct = function(tag) {
      vm.requirement.products.splice(vm.requirement.products.indexOf(tag), 1)
    }

    vm.noneProductsThatApplies = function() {
      return !vm.requirement.products || vm.requirement.products.length == 0
    }

    vm.openProductChooser = function(size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/product-add-modal.html',
        controller: 'ProductAddModalController',
        controllerAs: 'mod',
        size: size,
        resolve: {
          items: function() {
            return vm.requirement.products;
          }
        }
      });

      modalInstance.result.then(function(selectedItem) {
        vm.selected = selectedItem;
        if (vm.selected) {
          if (!vm.requirement.products) {
            vm.requirement.products = []
          }
          vm.requirement.products = vm.requirement.products.concat(vm.selected)
        }
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    //Tags
    vm.removeTag = function(tag) {
      vm.requirement.tags.splice(vm.requirement.tags.indexOf(tag), 1)
    }

    vm.openTagsChooser = function(size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/tag-add-modal.html',
        controller: 'TagAddModalController',
        controllerAs: 'mod',
        size: size,
        resolve: {
          items: function() {
            return vm.requirement.tags;
          }
        }
      });

      modalInstance.result.then(function(selectedItem) {
        vm.selected = selectedItem;
        if (vm.selected) {
          if (!vm.requirement.tags) {
            vm.requirement.tags = []
          }
          vm.requirement.tags = vm.requirement.tags.concat(vm.selected)
        }
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    //Level
    vm.noneLevelThatApplies = function() {
      return vm.requirement.level == null
    }

    vm.openLevelChooser = function(size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/level-select-modal.html',
        controller: 'LevelSelectModalController',
        controllerAs: 'mod',
        size: size,
        resolve: {
          item: function() {
            return vm.requirement.level;
          }
        }
      });

      modalInstance.result.then(function(selectedItem) {
        vm.selected = selectedItem;
        if (vm.selected)
          vm.requirement.level = (vm.selected)
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    }

    //Lo Level
    vm.lowestReqLevel = function() {
      console.log("WTF WTF")
      return levelService.isLowestLevel(vm.requirement.level)
    }

    vm.removeLoLevelReq = function(req) {
      vm.requirement.loLevel.splice(vm.requirement.loLevel.indexOf(req), 1)
    }

    vm.openLoLevelChooser = function(size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/lo-level-select-modal.html',
        controller: 'LoLevelSelectModalController',
        controllerAs: 'vm',
        size: size,
        resolve: {
          requirement: function() {
            return vm.requirement;
          }
        }
      });
      modalInstance.result.then(function(selectedItem) {
        vm.selected = selectedItem;
        if (vm.selected) {
          if (!vm.requirement.loLevel) {
            vm.requirement.loLevel = []
          }
          vm.requirement.loLevel.push(vm.selected)
        }
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };


    vm.openReqPreview = function(req) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'app/requirements/req-view-modal.html',
        controller: 'ReqViewModalController',
        controllerAs: 'vm',
        size: 'lg',
        resolve: {
          requirement: function() {
            return req;
          }
        }
      });
    };




  }
})();
