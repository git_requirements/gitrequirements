(function() {
  // Please note that $modalInstance represents a modal window (instance) dependency.
  // It is not the same as the $uibModal service used above.

  angular.module('gitReqs').controller('RemoveReqsModalController', ['filterService', '$uibModalInstance', RemoveReqsModalController]);

  function RemoveReqsModalController(filterService, $uibModalInstance) {
    var mod = this

    mod.ok = function() {
      $uibModalInstance.close();
    };

    mod.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };
  };
})()
