(function() {
  // Please note that $modalInstance represents a modal window (instance) dependency.
  // It is not the same as the $uibModal service used above.

  angular.module('gitReqs').controller('TagAddModalController', ['filterService', '$uibModalInstance', 'items', TagAddModalController]);

  function TagAddModalController(filterService, $uibModalInstance, items) {
    var mod = this
    mod.notAppliedTags = []
    mod.notAppliedTags = filterService.getAllTags()
    mod.notAppliedTags = array_diff(mod.notAppliedTags, items)

    mod.items = [];
    mod.items = mod.notAppliedTags;
    mod.selected = []

    mod.ok = function() {
      $uibModalInstance.close(mod.selected);
    };

    mod.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };

    mod.addSelected = function(item) {
      mod.selected.push(item)
      mod.items = array_diff(mod.items, mod.selected)
    }

  };

  function array_diff(arr1) {
    var retArr = {}
    argl = arguments.length,
      k1 = '',
      i = 1,
      k = '',
      arr = {};

    arr1keys: for (k1 in arr1) {
      for (i = 1; i < argl; i++) {
        arr = arguments[i];
        for (k in arr) {
          if (arr[k].id === arr1[k1].id) {
            // If it reaches here, it was found in at least one array, so try next value
            continue arr1keys;
          }
        }

        retArr[k1] = arr1[k1];
      }
    }

    return retArr;
  }

})()
