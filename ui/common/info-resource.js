(function() {
    "use strict"
    angular.module("common.services").factory("infoResource", ["$resource", infoResource]);

    function infoResource($resource) {
        return $resource("/info/");
    }

})();
