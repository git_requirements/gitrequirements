(function() {
    "use strict"
    angular.module("common.services").factory("requirementsResource", ["$resource", requirementsResource]);

    function requirementsResource($resource) {
        return $resource("/requirements/");
    }

})();