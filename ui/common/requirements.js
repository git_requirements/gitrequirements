var reqs = [{
  "id": 1,
  "description": "Lorem ipsum...",
  "name": "Login page requirements",
  "level": {
    "id": 1,
    "name": "L1"

  },
  "loLevel": [{
    "id": 2,
    "name": "Hello popup"
  }, {
    "id": 3,
    "name": "Password requirements"
  }, {
    "id": 4,
    "name": "User management"
  }],
  "products": [{
    "id": 1,
    "name": "AuC"
  }, {
    "id": 2,
    "name": "KMF"
  }, {
    "id": 3,
    "name": "CMF"
  }],
  "releases": [{
    "id": 1,
    "name": "A7.16"
  }, {
    "id": 2,
    "name": "A7.15"
  }, {
    "id": 3,
    "name": "D8.3"
  }],
  "tags": [{
    "id": 1,
    "name": "Login"
  }, {
    "id": 2,
    "name": "Logout"
  }]
}, {
  "id": 2,
  "name": "Hello popup",
  "level": {
    "id": 2,
    "name": "L2"
  },
  "hiLevel": {
    "id": 1,
    "name": "Login page requirements",
  },
  "loLevel": [],
  "products": [{
    "id": 1,
    "name": "AuC"
  }, {
    "id": 2,
    "name": "KMF"
  }, {
    "id": 3,
    "name": "CMF"
  }],
  "releases": [{
    "id": 1,
    "name": "A7.16"
  }, {
    "id": 2,
    "name": "A7.15"
  }, {
    "id": 3,
    "name": "D8.3"
  }],
  "tags": [{
    "id": 1,
    "name": "Login"
  }, {
    "id": 2,
    "name": "Logout"
  }, {
    "id": 3,
    "name": "View"
  }]
}, {
  "id": 3,
  "name": "Password requirements",
  "level": {
    "id": 2,
    "name": "L2"
  },
  "hiLevel": {
    "id": 1,
    "name": "Login page requirements",
  },
  "loLevel": [],
  "products": [{
    "id": 1,
    "name": "AuC"
  }, {
    "id": 2,
    "name": "KMF"
  }, {
    "id": 3,
    "name": "CMF"
  }],
  "releases": [{
    "id": 1,
    "name": "A7.16"
  }, {
    "id": 2,
    "name": "A7.15"
  }, {
    "id": 3,
    "name": "D8.3"
  }]
}, {
  "id": 4,
  "name": "User management",
  "level": {
    "id": 2,
    "name": "L2"
  },
  "hiLevel": {
    "id": 1,
    "name": "Login page requirements",
  },
  "loLevel": [{
    "id": 5,
    "name": "Session timeout"
  }],
  "products": [{
    "id": 1,
    "name": "AuC"
  }, {
    "id": 2,
    "name": "KMF"
  }, {
    "id": 3,
    "name": "CMF"
  }],
  "releases": [{
    "id": 1,
    "name": "A7.16"
  }, {
    "id": 2,
    "name": "A7.15"
  }, {
    "id": 3,
    "name": "D8.3"
  }]
}, {
  "id": 5,
  "name": "Session timeout",
  "level": {
    "id": 3,
    "name": "L3"
  },
  "hiLevel": {
    "id": 4,
    "name": "User management"
  },
  "loLevel": [{
    "id": 6,
    "name": "Logout page"
  }],
  "products": [{
    "id": 1,
    "name": "AuC"
  }, {
    "id": 2,
    "name": "KMF"
  }, {
    "id": 3,
    "name": "CMF"
  }],
  "releases": [{
    "id": 1,
    "name": "A7.16"
  }, {
    "id": 2,
    "name": "A7.15"
  }, {
    "id": 3,
    "name": "D8.3"
  }]
}, {
  "id": 6,
  "name": "Logout page",
  "level": {
    "id": 4,
    "name": "L4"
  },
  "hiLevel": {
    "id": 5,
    "name": "Session timeout"
  },
  "loLevel": [],
  "products": [{
    "id": 1,
    "name": "AuC"
  }, {
    "id": 2,
    "name": "KMF"
  }, {
    "id": 3,
    "name": "CMF"
  }],
  "releases": [{
    "id": 6,
    "name": "A7.13"
  }, {
    "id": 7,
    "name": "D8.0"
  }]
}, {
  "id": 7,
  "name": "Logout page layout",
  "level": {
    "id": 4,
    "name": "L4"
  },
  "hiLevel": {},
  "loLevel": [],
  "products": [{
    "id": 1,
    "name": "AuC"
  }, {
    "id": 2,
    "name": "KMF"
  }, {
    "id": 3,
    "name": "CMF"
  }],
  "releases": [{
    "id": 6,
    "name": "A7.13"
  }, {
    "id": 7,
    "name": "D8.0"
  }]
}]


var mockReleases = [{
  "id": 1,
  "name": "A7.16"
}, {
  "id": 2,
  "name": "A7.15"
}, {
  "id": 3,
  "name": "D8.3"
}, {
  "id": 4,
  "name": "D8.2"
}, {
  "id": 5,
  "name": "D8.1"
}, {
  "id": 6,
  "name": "A7.13"
}, {
  "id": 7,
  "name": "D8.0"
}]


var mockProducts = [{
    "id": 1,
    "name": "AuC"
}, {
    "id": 2,
    "name": "KMF"
}, {
    "id": 3,
    "name": "CMF"
}, {
    "id": 4,
    "name": "KVL"
}, {
    "id": 5,
    "name": "Common Web"
}, {
    "id": 6,
    "name": "Standy"
}, {
    "id": 7,
    "name": "License Manager"
}, {
    "id": 8,
    "name": "SPA"
}, {
    "id": 9,
    "name": "ZDS"
}, {
    "id": 10,
    "name": "UEM"
}, {
    "id": 11,
    "name": "UNC"
}, {
    "id": 12,
    "name": "Zone Watch"
}, {
    "id": 13,
    "name": "PTT"
}, {
    "id": 14,
    "name": "Conosle"
}]


var mockTags = [{
    "id": 1,
    "name": "Login"
}, {
    "id": 2,
    "name": "Logout"
}, {
    "id": 3,
    "name": "View"
}, {
    "id": 4,
    "name": "Security"
}, {
    "id": 5,
    "name": "Session"
}, {
    "id": 6,
    "name": "Layout"
}, {
    "id": 7,
    "name": "Users"
}]
