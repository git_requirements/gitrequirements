(function() {
  "use strict"
  angular.module("common.services").factory("productsResource", ["$resource", productsResource]);

  function productsResource($resource) {
    return $resource("/products/");
  }

})();
