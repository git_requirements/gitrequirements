(function() {
  "use strict"
  var app = angular.module("infoResourceMock", ["ngMockE2E"]);

  app.run(function($httpBackend) {
    $httpBackend.whenGET(/app/).passThrough();
    $httpBackend.whenGET(/requirements/).passThrough();

    var requirements = reqs;

    var reUrlProperty = /[a-z]+=[0-9]+/g;
    var reqNrUrl = new RegExp("/info\\?")

    var any = new RegExp("[.]*")


    $httpBackend.whenGET(any).respond(function(method, url, data) {
      console.log("URL: " + url)
      return [200, requirements.length, {}]
    });

    $httpBackend.whenGET(reqNrUrl).respond(function(method, url, data) {
      console.log("URL: " + url)
      return [200, requirements.length, {}]
    });


  })
})();
