(function() {
  "use strict"
  var app = angular.module("requirementsResourceMock", ["ngMockE2E"]);

  app.run(function($httpBackend) {
    $httpBackend.whenGET(/app/).passThrough();
    //$httpBackend.whenGET(/info/).passThrough();


    var requirements = reqs;
    var releases = mockReleases;
    var products = mockProducts;
    var tags = mockTags;



    var reUrlArray = /[a-z]{3}=%5B[0-9|,]*%5D/g;
    var reUrlProperty = /[a-z]+=[0-9|a-z|A-Z]+/g;

    var reqsUrl = new RegExp("/requirements\\?" + "[a-z]+=[0-9]+&")
    var reqIdUrl = new RegExp("/requirements\\?" + "id=[0-9]+")

    var releasesNrUrl = new RegExp("/releases\\?nr")
    var releasesUrl = new RegExp("/releases")
    var releaseDetails = new RegExp("/releases\\?id=" + "[0-9][0-9]*$")


    var productsNrUrl = new RegExp("/products\\?nr")
    var productsUrl = new RegExp("/products")

    var reqDetails = new RegExp("/requirements\\?id=" + "[0-9][0-9]*$")
    var requirementsEditUrl = new RegExp("/requirements" + "/[0-9][0-9]*$", '');

    var tagsNrUrl = new RegExp("/tags\\?nr")
    var tagsUrl = new RegExp("/tags")

    function getFilter(filter, filtersArray) {
      var filterIds = [];
      filtersArray.forEach(function(entry) {
        if (entry.substring(0, 3) == filter) {
          entry = entry.replace(filter + "=%5B", "");
          entry = entry.replace("%5D", "");
          filterIds = entry.match(/[0-9|,]*/g)[0].split(",");
        }
      });
      return filterIds;
    }

    function getProperty(property, propertiesArray) {
      var result = ""
      propertiesArray.forEach(function(entry) {
        if (entry.substring(0, entry.indexOf("=")) == property) {
          result = entry.substring(entry.indexOf("=") + 1);
        }
      });
      return result;
    }

    $httpBackend.whenGET(reqDetails).respond(function(method, url, data) {
      var requirement = requirements[{}]
      var idParam = url.split('?')[1];
      var id = idParam.split("=")[1]
      console.log("URL: " + url)

      if (id > 0) {
        for (var i = 0; i < requirements.length; i++) {

          if (requirements[i].id == id) {
            requirement = requirements[i];
            break;
          }
        }
      }

      return [200, requirement, {}]
    });

    $httpBackend.whenDELETE(reqIdUrl).respond(function(method, url, data) {
      console.log("URL: " + url)
      var idParam = url.split('?')[1];
      var id = idParam.split("=")[1]

      for (var req of requirements) {
        if (req.id == id) {
          requirements.splice(requirements.indexOf(req), 1)
        }
      }
      console.log(requirements)
      return [200, {
        requirements
      }, {}]
    });


    $httpBackend.whenGET(reqsUrl).respond(function(method, url, data) {
      var filtersArray = url.match(reUrlArray);
      console.log("URL: " + url)
      var prdFilters = getFilter("prd", filtersArray)
      var relFilters = getFilter("rel", filtersArray)
      var lvlFilters = getFilter("lvl", filtersArray)

      var propertiesArray = url.match(reUrlProperty);
      var begin = parseInt(getProperty("begin", propertiesArray))
      var limit = parseInt(getProperty("limit", propertiesArray))
      var query = getProperty("query", propertiesArray)
      var first = (begin - 1) * limit
      var last = begin * limit

      var foundReqs = [];
      for (var i = 0; i < requirements.length; i++) {
        //for (var j = 0; j < lvlFilters.length; j++) {

        //  if (lvlFilters[j] == requirements[i].level.id) {
        //    if (foundReqs.indexOf(requirements[i]) < 0) {
        if (!query || query.length == 0) {
          foundReqs.push(requirements[i])
        } else if (query && ((requirements[i].name.indexOf(query) > -1) || (requirements[i].description && requirements[i].description.indexOf(query) > -1))) {
          foundReqs.push(requirements[i])
        }
        //      }
        //  }
        //  }
      }
      //for development only:
      //foundReqs = requirements;
      foundReqs = foundReqs.slice(first, last)
      console.log(foundReqs)
      console.log(JSON.stringify(foundReqs))
      return [200, foundReqs, {}]
    });

    $httpBackend.whenGET(releasesNrUrl).respond(function(method, url, data) {
      console.log("URL: " + url)
      return [200, [releases.length], {}]
    });

    $httpBackend.whenGET(releasesUrl).respond(function(method, url, data) {
      var propertiesArray = url.match(reUrlProperty);
      var begin = parseInt(getProperty("begin", propertiesArray))
      var limit = parseInt(getProperty("limit", propertiesArray))
      var query = getProperty("query", propertiesArray)
      var first = (begin - 1) * limit
      var last = begin * limit

      console.log("URL: " + url)
      return [200, releases.slice(first, last), {}]
    });

    $httpBackend.whenGET(releaseDetails).respond(function(method, url, data) {
      var release = releases[{}]
      var idParam = url.split('?')[1];
      var id = idParam.split("=")[1]
      console.log("URL: " + url)

      if (id > 0) {
        for (var i = 0; i < releases.length; i++) {

          if (releases[i].id == id) {
            release = releases[i];
            break;
          }
        }
      }

      return [200, release, {}]
    });

    $httpBackend.whenPOST(releasesUrl).respond(function(method, url, data) {
      console.log(data)
      var release = angular.fromJson(data);
      if (!release.id) {
        if (releases.length > 0) {
          release.id = releases[releases.length - 1].id + 1;
        } else {
          release.id = 1
        }
        releases.push(release)
      } else {
        for (var i = 0; i < releases.length; i++) {
          if (releases[i].id == release.id) {
            releases[i] = release;
            break;
          }
        }
      }
      return [200, release, {}];
    });

    $httpBackend.whenGET(productsNrUrl).respond(function(method, url, data) {
      console.log("URL: " + url)
      return [200, [products.length], {}]
    });

    $httpBackend.whenGET(productsUrl).respond(function(method, url, data) {
      var propertiesArray = url.match(reUrlProperty);
      var begin = parseInt(getProperty("begin", propertiesArray))
      var limit = parseInt(getProperty("limit", propertiesArray))
      var query = getProperty("query", propertiesArray)
      var first = (begin - 1) * limit
      var last = begin * limit

      console.log("URL: " + url)
      return [200, products.slice(first, last), {}]
    });

    $httpBackend.whenDELETE(productsUrl).respond(function(method, url, data) {
      console.log("URL: " + url)
      var idParam = url.split('?')[1];
      var id = idParam.split("=")[1]

      for (var product of products) {
        if (product.id == id) {
          products.splice(products.indexOf(product), 1)
        }
      }

      return [200, {
        products
      }, {}]
    });


    $httpBackend.whenPOST(productsUrl).respond(function(method, url, data) {
      console.log(data)
      var product = angular.fromJson(data);
      if (!product.id) {
        if (products.length > 0) {
          product.id = products[products.length - 1].id + 1;
        } else {
          product.id = 1
        }
        products.push(product)
      } else {
        for (var i = 0; i < products.length; i++) {
          if (products[i].id == product.id) {
            products[i] = product;
            break;
          }
        }
      }
      return [200, product, {}];
    });

    $httpBackend.whenGET(tagsNrUrl).respond(function(method, url, data) {
      console.log("URL: " + url)
      return [200, [tags.length], {}]
    });

    $httpBackend.whenGET(tagsUrl).respond(function(method, url, data) {
      var propertiesArray = url.match(reUrlProperty);
      var begin = parseInt(getProperty("begin", propertiesArray))
      var limit = parseInt(getProperty("limit", propertiesArray))
      var first = (begin - 1) * limit
      var last = begin * limit

      console.log("URL: " + url)
      return [200, tags.slice(first, last), {}]
    });

    $httpBackend.whenDELETE(tagsUrl).respond(function(method, url, data) {
      console.log("URL: " + url)
      var idParam = url.split('?')[1];
      var id = idParam.split("=")[1]

      for (var tag of tags) {
        if (tag.id == id) {
          tags.splice(tags.indexOf(tag), 1)
        }
      }

      return [200, {
        tags
      }, {}]
    });

    $httpBackend.whenPOST(tagsUrl).respond(function(method, url, data) {
      console.log(data)
      var tag = angular.fromJson(data);
      if (!tag.id) {
        if (tags.length > 0) {
          tag.id = tags[tags.length - 1].id + 1;
        } else {
          tag.id = 1
        }
        tags.push(tag)
      } else {
        for (var i = 0; i < tags.length; i++) {
          if (tags[i].id == tag.id) {
            tags[i] = tag;
            break;
          }
        }
      }
      return [200, tag, {}];
    });


    var any = new RegExp("[.]*")


    $httpBackend.whenGET(any).respond(function(method, url, data) {
      console.log("URL: " + url)
      return [200, [requirements.length], {}]
    });

    $httpBackend.whenPOST("/requirements").respond(function(method, url, data) {
      console.log(data)
      var requirement = angular.fromJson(data);
      if (!requirement.id) {
        if (requirements.length > 0) {
          requirement.id = requirements[requirements.length - 1].id + 1;
        } else {
          requirement.id = 1
        }
        requirements.push(requirement)
      } else {
        for (var i = 0; i < requirements.length; i++) {
          if (requirements[i].id == requirement.id) {
            requirements[i] = requirement;
            break;
          }
        }
      }
      return [200, requirement, {}];
    });
  })
})();
