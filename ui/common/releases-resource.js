(function() {
    "use strict"
    angular.module("common.services").factory("releasesResource", ["$resource", releasesResource]);

    function releasesResource($resource) {
        return $resource("/releases/");
    }

})();
