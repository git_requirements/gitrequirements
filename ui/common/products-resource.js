(function() {
  "use strict"
  angular.module("common.services").factory("tagsResource", ["$resource", tagsResource]);

  function tagsResource($resource) {
    return $resource("/tags/");
  }

})();
